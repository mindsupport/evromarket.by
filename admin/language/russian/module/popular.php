<?php
// Heading
$_['heading_title']       = 'Популярные продукты (недавно просмотренные)';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Настройки сохранены!';
$_['text_content_top']    = 'Вверху';
$_['text_content_bottom'] = 'Внизу';
$_['text_column_left']    = 'Левая колонка';
$_['text_column_right']   = 'Правая колонка';

// Entry
$_['entry_limit']         = 'Кол-во:';
$_['entry_image']         = 'Картинка (WxH):';
$_['entry_layout']        = 'Шаблон:';
$_['entry_position']      = 'Позиция:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Сортировка:';

// Error
$_['error_permission']    = 'Действие запрещено';
$_['error_image']         = 'Не заполнены ширина и высота картинки';
?>
