<?php
// Heading
$_['heading_title']     = 'Импорт продуктов';

// Text
$_['text_success']      = 'Категории и товары успешно добавлены!';
$_['text_nochange']     = 'Изменений не было.';
$_['text_log_details']  = 'Подробнее в файле ошибок.';

// Entry
$_['entry_restore']     = 'Импортировать из файла (xls,xlsx,csv):';
$_['entry_description'] = '';

// Error
$_['error_permission']          = 'У вас нет прав досутпа для изсопльзования этого плагина';
$_['error_upload']              = 'Не валидный файл';
$_['error_sheet_count']         = 'всего можно загрузить 8 страниц';
$_['error_categories_header']   = 'Неправильный заголовк в категориях';
$_['error_products_header']     = 'Неправильный заголовок в продуктах';
$_['error_options_header']      = 'неправильный заголовок в опциях продуктов';
$_['error_attributes_header']   = 'Неправильный заголовок в аттрибутах';
$_['error_specials_header']     = 'Неверные спациальные продукты';
$_['error_discounts_header']    = 'Дискаунты неправильные';
$_['error_rewards_header']      = 'Пообщрения неверные';
$_['error_select_file']         = 'Выберите файл для импорта';
$_['error_post_max_size']       = 'Размер больше чем %1 (post_max_size - максимум в конфиге)';
$_['error_upload_max_filesize'] = 'Размер файла больше чем %1 (upload_max_filesize - конфиг)';

// Button labels
$_['button_import']     = 'Импорт из файла';
$_['button_export']     = 'Экспорт';
?>