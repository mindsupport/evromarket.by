
<?php

include('export/header.tpl'); 

if(!empty($this->session->data['csv_columns'])) {
    $h1 = 'Назначение полей';
    include('export/bind_fields_form.tpl');
} else {
    $h1 = 'Импорт из файла';
    $upload_field_text = 'Выберите файл (csv):';
    include('export/upload_form.tpl');
}



include('export/footer.tpl'); 

?>