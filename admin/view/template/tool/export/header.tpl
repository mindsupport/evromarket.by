<?php

echo $header; ?>
<div id="content">
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
    </div>
  <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>

    <?php if (!empty($error)) { ?>
    <div class="warning">
    <?php foreach($error as $key => $error_data): ?>
    <?php foreach($error_data as $index => $error_string): ?>
    <?php echo $error_string; ?><br>
    <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
  <?php } ?>

  <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
  <?php } ?>

    <?php if(!empty($out)): ?>

    <div class="success">
        <b>Импорт прошел успешно!</b>
        <br>

        Всего новых категорий: <?php echo count($out['new_categories']); ?><br>
        Обновлено категорий: <?php echo count($out['updated_categories']); ?><br>
        Новых продуктов: <?php echo count($out['new_products']); ?><br>
        Обновлено продуктов: <?php echo count($out['updated_products']); ?><br>

    </div>

    <?php endif; ?>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/backup.png" alt="" /><?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a href="<?php echo $import_url; ?>" class="button"><span><?php echo $button_import; ?></span></a>
                <a href="<?php echo $config_url; ?>" class="button"><span>Настройки автоматического обновления</span></a>
            </div>

        </div>
        <div class="content">
            Файл должен быть формата <a href="https://ru.wikipedia.org/wiki/CSV" target="_blank">CSV</a>. Обязательные колонки, которые должны существовать в файле: имя категории, модель продукта (уникальная строка для продукта), производитель, имя продукта.
