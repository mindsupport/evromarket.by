</div>
</div>
</div>
<script type="text/javascript"><!--
function checkFileSize(id) {
        // See also http://stackoverflow.com/questions/3717793/javascript-file-upload-size-validation for details
        var input, file, file_size;

        if (!window.FileReader) {
            // The file API isn't yet supported on user's browser
            return true;
        }

        input = document.getElementById(id);
        if (!input) {
            // couldn't find the file input element
            return true;
        }
        else if (!input.files) {
            // browser doesn't seem to support the `files` property of file inputs
            return true;
        }
        else if (!input.files[0]) {
            // no file has been selected for the upload
            alert("<?php echo $error_select_file; ?>");
            return false;
        }
        else {
            file = input.files[0];
            file_size = file.size;
                <?php if (!empty($post_max_size)) { ?>
            // check against PHP's post_max_size
            post_max_size = <?php echo $post_max_size; ?>;
            if (file_size > post_max_size) {
                alert("<?php echo $error_post_max_size; ?>");
                return false;
            }
                <?php } ?>
                <?php if (!empty($upload_max_filesize)) { ?>
            // check against PHP's upload_max_filesize
            upload_max_filesize = <?php echo $upload_max_filesize; ?>;
            if (file_size > upload_max_filesize) {
                alert("<?php echo $error_upload_max_filesize; ?>");
                return false;
            }
                <?php } ?>
            return true;
        }
    }

    function upload() {
        if (checkFileSize('upload')) {
            $('#form').submit();
        }
    }
//--></script>
<?php echo $footer; ?>