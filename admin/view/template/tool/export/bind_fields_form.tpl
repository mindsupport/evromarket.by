<?php ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

    <?php foreach($columns as $columnType => $allData): ?>
    <h1><?php echo $sections[$columnType]['_' . $columnType . '_name']; ?></h1>
    <table class="form">    
        <tr>
        <?php foreach($allData as $index => $column): ?>

            <td>
                <?php
                if(!is_array($column['name'])) {
                    $names = preg_split('/\|/', $column['name']);
                } else {
                    $names = preg_split('/\|/', $column['name']['name']);
                }
            ?>

                <span style="display: block;" <?php if(!empty($error) && isset($error[$columnType][$column['key']])): ?>class="error"<?php endif; ?>>  
                <?php echo $names[0]; ?>
                    <?php  if(is_array($column['name']) && isset($column['name']['required'])): ?><sup style="color: red;">*</sup>
                    <?php endif; ?>
                </span>

                <br> 
                <?php echo $column['select']; ?>
                <br>
        <quote style="display:block; width: 180px; margin-top: 5px;font-size: 10px; color: #acacac; font-style: italic;min-height: 25px;">
          <?php if(!empty($names[1])): ?>
        <?php echo $names[1]; ?>
                <?php endif; ?>
        </quote>
        </td> 

                <?php if($index > 0 && ($index+1)%5 == 0): ?>
        </tr>
    <?php if(count($allData) != $index+1): ?><tr><?php endif; ?>
                <?php endif; ?>

        <?php endforeach; ?>
        </tr> </table>


        <?php endforeach; ?>

    <table class="form">
        <tr>
            <td>                  
                <span <?php if(isset($error['products']['image_delimiter'])): ?>class="error" style="display:inline-block;"<?php endif; ?>>Разделитель поля "URL Картинки": </span>
                
                <input type="text" name="image_delimiter"  value="<?php if(!empty($this->request->request['image_delimiter'])): ?><?php echo $this->request->request['image_delimiter']; ?><?php else:?>;<?php endif; ?>" /> 
            </td>
        </tr>
        <tr>
            <td>Архив с картинками: <input type="file" name="zip_photos_archive" /> 
        <quote style="display:block; margin-top: 5px;font-size: 10px; color: #acacac; font-style: italic;">
            Файл zip формата с картинками внутри
        </quote>
        </td>
        </tr>
        <tr>
            <td>Удалить Категории и товары?: <input type="checkbox" name="reset_shop"  value="1" <?php if(!empty($this->request->request['reset_shop'])): ?>checked="checked"<?php endif; ?> /> 
            </td>
        </tr>
    </table>
    <button>Сохранить</button>
</form>