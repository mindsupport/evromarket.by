<?php

class ControllerToolExport extends Controller {

    private $error = array();

    public function __construct($registry) {
        parent::__construct($registry);

        $this->load->language('tool/export');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('tool/export');
    }

    public function process() {

        $this->get_warning();
        $this->get_breadcrumbs();

        $this->data['sections'] = $this->validNames();
        $this->data['action'] = $this->url->link('tool/export/process', 'token=' . $this->session->data['token'], 'SSL');

        if (!empty($this->request->post['reset_shop'])) {
            $this->model_tool_export->resetShop();
        }

        if (!empty($this->session->data['csv_columns'])) {
            $this->bindColumns($this->session->data['csv_columns']);
        } else {
            $this->redirect($this->url->link('tool/export', 'token=' . $this->request->get['token'], 'SSL'));
        }

        if (!empty($this->request->post['columns']) && $this->validateProcess()) {
            $columns = $this->request->post['columns'];
            $files = array();

            if (!empty($this->request->files['zip_photos_archive']) &&
                    (int) $this->request->files['zip_photos_archive']['error'] == 0 &&
                    (int) $this->request->files['zip_photos_archive']['size'] > 0 && $this->request->files['zip_photos_archive']['type'] == 'application/zip') {


                $zip = new ZipArchive;

                $zipResource = $zip->open($this->request->files['zip_photos_archive']['tmp_name']);
                $path = getenv('DOCUMENT_ROOT') . '/image/data/unzipped';


                if ($zipResource === TRUE) {
                    $zip->extractTo($path);
                    $zip->close();
                    $files = array_filter(glob($path . '/*/*'), 'is_file');
                }
            }

            $allData = array();

            if (!empty($columns)) {
                require(DIR_SYSTEM . 'PHPExcel/Classes/PHPExcel.php');

                $inputFileType = PHPExcel_IOFactory::identify($this->session->data['csv_file_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);

                if ($objReader instanceof PHPExcel_Reader_CSV) {

                    $objReader->setDelimiter($this->session->data['delimiter']);
                    $data = $objReader->load($this->session->data['csv_file_name']);

                    $sheet = $data->getSheet();
                    $rows = $sheet->getHighestRow();
                    $isFirstRow = true;

                    for ($i = 0; $i < $rows; $i++) {
                        if ($isFirstRow) {
                            $isFirstRow = false;
                            continue;
                        }

                        foreach ($columns as $section => $sectionData) {

                            foreach ($sectionData as $key => $cellColumnIndex) {
                                if (!empty($cellColumnIndex)) {
                                    $allData[$section][$i][$key] = $this->model_tool_export->getCell($sheet, $i, $cellColumnIndex);
                                } else {
                                    $allData[$section][$i][$key] = false;
                                }
                            }
                        }
                    }
                }
                $allData['files'] = $files;

                if (!empty($allData)) {
                    $out = $this->model_tool_export->storeCategories($allData);
                    $this->data['out'] = $out;
                } else {
                    $this->error['warning'] = '';
                }
            }
        }

        $this->template = 'tool/export/process.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        $this->response->setOutput($this->render());
    }

    protected function validNames() {

        return array(
            'categories' => array(
                'name' => array('name' => 'Имя:', 'required' => true),
//                'category_id' => 'ID категории:',
                'image' => 'URL Картинки:|Принимаются URL с разделителем (можно указать ниже) или без',
                'parent_id' => 'Родительская категория:',
                'meta_description' => 'Мета описание:',
                'meta_keyword' => 'Ключевые слова:',
                'seo_keyword' => 'URL Алиас:',
                'description' => 'Описание:',
                'sort_order' => '# по порядку:',
//                'top' => 'В верхнее меню:',
                '_categories_name' => 'Поля для категорий'
            ),
            'products' => array(
                'name' => array('name' => 'Имя:', 'required' => true),
                'manufacturer' => array('name' => 'Производитель:', 'required' => true),
                'model' => array('name' => 'Модель:|Уникальная строка для каждого продукта', 'required' => true),
                'price' => array('name' => 'Цена:|в USD', 'required' => true),
                'related' => array('name' => 'Варианты товара:|Если будут найдены товары с одинаковыми строками в этой колонке, то они будут группироваться'),
                'quantity' => 'Кол-во:|По умолчанию- 100',
                'image' => 'URL Картинки:|Принимаются URL с разделителем (можно указать ниже) или без',
                'shipping' => 'Доставка:|Принимает значения 0 или 1, по умолчанию - 1',
                'description' => 'Описание:',
                'meta_description' => 'Мета описание:',
                'seo_keyword' => 'URL алиас:',
                'meta_keywords' => 'Ключевые слова:',
                '_products_name' => 'Продукты:',
                'sku' => 'SKU:',
                'upc' => 'UPC:',
                'ean' => 'EAN:',
                'jan' => 'JAN'
            )
        );
    }

    protected function setColumns($columnArray, $name, $section) {

        $validNames = $this->validNames();

        $select = '<select name="columns[' . $section . '][' . $name . ']">';
        $select .= '<option value=""> Выберите поле... </option>';

        foreach ($columnArray as $index => $col) {
            $select .= '<option value="' . $index . '" ';

            if (!empty($this->request->post) && !empty($this->request->post['columns'][$section][$name]) && $index == (int) $this->request->post['columns'][$section][$name]) {
                $select .= 'selected="selected"';
            }

            $select .= '>' . $col . '</option>';
        }

        $select .= '</select>';

        return array('select' => $select, 'name' => $validNames[$section][$name], 'key' => $name);
    }

    protected function sections() {
        return array(
            'category' => 'Поля для категорий'
        );
    }

    public function index() {
        $this->get_breadcrumbs();
        $this->data['sections'] = $this->validNames();

        $this->session->data['csv_columns'] = array();
        $this->data['columns'] = array();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
            $this->data['action'] = $this->url->link('tool/export/process', 'token=' . $this->session->data['token'], 'SSL');

            if ((isset($this->request->files['upload'])) && (is_uploaded_file($this->request->files['upload']['tmp_name']))) {


                $path = getenv('DOCUMENT_ROOT') . '/csv/' . time() . '_' . $this->request->files['upload']['name'];

                if (move_uploaded_file($this->request->files['upload']['tmp_name'], $path)) {
                    $this->session->data['csv_file_name'] = $path;
                    $this->session->data['delimiter'] = $this->request->post['delimiter'];
                }

                $this->session->data['csv_columns'] = $this->model_tool_export->getCsvColumns($path, $this->request->request);

                if (!empty($this->session->data['csv_columns'])) {

                    $this->bindColumns($this->session->data['csv_columns']);
                } else {
                    $this->error['warning'] = 'Колонки не найдены или неверный формат файла';
                }
            } else {
                $this->error['warning'] = $this->language->get('error_upload');
            }
        }

        $this->get_warning();


        $this->template = 'tool/export.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );
        $this->response->setOutput($this->render());
    }

    function bindColumns($columns) {
        foreach ($this->validNames() as $sectionAlias => $sectionInfo) {
            $this->data['columns'][$sectionAlias] = array();
            foreach ($sectionInfo as $alias => $aliasName) {
                if ($alias !== '_' . $sectionAlias . '_name') {
                    $this->data['columns'][$sectionAlias][] = $this->setColumns($columns, $alias, $sectionAlias);
                }
            }
        }
    }

    function return_bytes($val) {
        $val = trim($val);

        switch (strtolower(substr($val, -1))) {
            case 'm': $val = (int) substr($val, 0, -1) * 1048576;
                break;
            case 'k': $val = (int) substr($val, 0, -1) * 1024;
                break;
            case 'g': $val = (int) substr($val, 0, -1) * 1073741824;
                break;
            case 'b':
                switch (strtolower(substr($val, -2, 1))) {
                    case 'm': $val = (int) substr($val, 0, -2) * 1048576;
                        break;
                    case 'k': $val = (int) substr($val, 0, -2) * 1024;
                        break;
                    case 'g': $val = (int) substr($val, 0, -2) * 1073741824;
                        break;
                    default : break;
                } break;
            default: break;
        }
        return $val;
    }

    public function configuration() {
        $this->get_breadcrumbs();
        $this->get_warning();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
            if ((isset($this->request->files['upload'])) && (is_uploaded_file($this->request->files['upload']['tmp_name']))) {
                $file = $this->request->files['upload']['tmp_name'];
                if ($this->model_tool_export->upload($file) === TRUE) {
//                    var_dump($file);
                    $this->session->data['success'] = $this->language->get('text_success');
                    $this->redirect($this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL'));
                } else {
                    $this->error['warning'] = $this->language->get('error_upload');
                }
            }
        }

        $this->data['action'] = $this->url->link('tool/export/configuration', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['import'] = $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['config_fields_map'] = unserialize($this->config->get('config_fields_map'));

        $this->template = 'tool/export/configuration.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );
        $this->response->setOutput($this->render());
    }

    public function download() {
        if ($this->validate()) {

            // send the categories, products and options as a spreadsheet file
            $this->load->model('tool/export');
            $this->model_tool_export->download();
            $this->redirect($this->url->link('tool/export', 'token=' . $this->request->get['token'], 'SSL'));
        } else {

            // return a permission error page
            return $this->forward('error/permission');
        }
    }

    private function validate() {

        if (!$this->user->hasPermission('modify', 'tool/export')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (empty($this->error)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function validateProcess() {
        $this->data['error'] = array();

        if (!$this->request->post['columns']['categories']['name']) {
            $this->data['error']['categories']['name'] = 'Колонка для имени категории не выбрана';
        }
        if (!$this->request->post['columns']['products']['name']) {
            $this->data['error']['products']['name'] = 'Колонка для имен продуктов не выбрана';
        }

        if (!$this->request->post['columns']['products']['model']) {
            $this->data['error']['products']['model'] = 'Модель для продукта не назначена';
        }

        if (!$this->request->post['columns']['products']['manufacturer']) {
            $this->data['error']['products']['manufacturer'] = 'Производитель отсутствует';
        }

        if (!$this->request->post['columns']['products']['price']) {
            $this->data['error']['products']['price'] = 'Не задано поле "Цена"';
        }

        if (!empty($this->session->data['delimiter']) && $this->session->data['delimiter'] == $this->request->post['image_delimiter']) {
            $this->data['error']['products']['image_delimiter'] = 'Разделитель для картинок совпадает с разделителем из CSV файла';
        }

        if (empty($this->data['error'])) {
            return true;
        } else {
            return false;
        }
    }

    private function get_warning() {
        if (!empty($this->session->data['export_error']['errstr'])) {
            $this->error['warning'] = $this->session->data['export_error']['errstr'];
            if (!empty($this->session->data['export_nochange'])) {
                $this->error['warning'] .= '<br />' . $this->language->get('text_nochange');
            }
            $this->error['warning'] .= '<br />' . $this->language->get('text_log_details');
        }
        unset($this->session->data['export_error']);
        unset($this->session->data['export_nochange']);

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['entry_restore'] = $this->language->get('entry_restore');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['button_import'] = $this->language->get('button_import');
        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['error_select_file'] = $this->language->get('error_select_file');
        $this->data['error_post_max_size'] = str_replace('%1', ini_get('post_max_size'), $this->language->get('error_post_max_size'));
        $this->data['error_upload_max_filesize'] = str_replace('%1', ini_get('upload_max_filesize'), $this->language->get('error_upload_max_filesize'));

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }
    }

    private function get_breadcrumbs() {
        $this->data['breadcrumbs'] = array();

        $this->data['config_url'] = $this->url->link('tool/export/configuration', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['import_url'] = $this->url->link('tool/export/index', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => FALSE
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['export'] = $this->url->link('tool/export/download', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['post_max_size'] = $this->return_bytes(ini_get('post_max_size'));
        $this->data['upload_max_filesize'] = $this->return_bytes(ini_get('upload_max_filesize'));
    }

}

?>