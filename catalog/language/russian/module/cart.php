<?php
// Heading 
$_['heading_title'] = 'Корзина';

// Text
$_['text_items']    = '<p>Товаров: %s</p> <p>%s</p>';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Посмотреть корзину';
$_['text_checkout'] = 'Оформление покупки';
$_['text_payment_profile'] = 'Платежный профиль';
?>