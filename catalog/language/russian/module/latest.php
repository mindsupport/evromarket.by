<?php
// Heading 
$_['heading_title'] = 'НОВИНКИ';
// Text
$_['text_reviews']  = 'На основании %s отзывов.'; 
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Название (А - Я)';
$_['text_name_desc']    = 'Название (Я - А)';
$_['text_price_asc']    = 'Цена (низкая &gt; высокая)';
$_['text_price_desc']   = 'Цена (высокая &gt; низкая)';
$_['text_rating_asc']   = 'Рейтинг (начиная с низкого)';
$_['text_rating_desc']  = 'Рейтинг (начиная с высокого)';
$_['text_model_asc']    = 'Модель (А- Я)';
$_['text_model_desc']   = 'Модель (Я - А)';
$_['text_manufacturer_asc']    = 'Производитель (А- Я)';
$_['text_manufacturer_desc']   = 'Производитель (Я - А)';
$_['text_limit']        = 'Показать:';
?>