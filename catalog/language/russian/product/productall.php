<?php
// Text
$_['text_refine']       = 'Изменить поиск';
$_['text_product']      = 'Каталог';
$_['text_error']        = 'Нет найдено ни одного продукта...';
$_['text_empty']        = 'Продуктов нет...';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Брэнд:';
$_['text_model']        = 'Кода:'; 
$_['text_points']       = 'Очки:'; 
$_['text_price']        = 'Цена:'; 
$_['text_tax']          = 'Налог:'; 
$_['text_reviews']      = ' %s отзывов.'; 
$_['text_compare']      = 'Стравнить (%s)'; 
$_['text_display']      = 'Отображение:';
$_['text_list']         = 'Список';
$_['text_grid']         = 'Сетка';
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Имя (А-Я)';
$_['text_name_desc']    = 'Имя (Я-А)';
$_['text_price_asc']    = 'Цена (по возрастанию)';
$_['text_price_desc']   = 'Цена (по убыванию)';
$_['text_rating_asc']   = 'Рейтинг (по возрастанию)';
$_['text_rating_desc']  = 'Рейтинг (по убыванию)';
$_['text_model_asc']    = 'Модель (А-Я)';
$_['text_model_desc']   = 'Модель (Я-А)';
$_['text_limit']        = 'Показать:';
?>