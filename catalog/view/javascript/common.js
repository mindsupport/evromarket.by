$(document).ready(function() {
    /* Search */
    $('.button-search').bind('click', function() {
        url = $('base').attr('href') + 'search/';

        var search = $('.search-from-head').val();

        if (search) {
            url += '?search=' + encodeURIComponent(search);
        }

        location = url;
    });

    $('.search-from-head').bind('keydown', function(e) {
        if (e.keyCode === 13) {
            url = $('base').attr('href') + 'search/';

            var search = $('.search-from-head').val();

            if (search) {
                url += '?search=' + encodeURIComponent(search);
            }

            location = url;
        }
    });

    $(".search-from-head").autocomplete({
        minLength: 0,
        source: "/search/",
        position: {
            my: "left top",
            at: "left bottom",
            of: ".container .field-wrap",
            within: ".container .field-wrap",
            collision: "flip",
            using: function(pos) {
                pos.width = "1088px";
                $(this).css(pos);
            }
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $('<li>')
                .append('<div class="search-item-wrap">')
                .append(item.thumb ? ('<div class="search-image"><a href="' + item.href + '"><img src="' + item.thumb + '"></a></div>') : '')
                .append('<a class="search-name" href="' + item.href + '">' + item.name + '</a><br>')
                .append(item.description.length > 0 ? ('<div class="search-description">' + item.description + '</div>') : '')
                .append('<div class="search-price"> Цена: ' + item.price + '</div>')
                .append('<div class="clear"></div>')
                .append('</div>')
                .appendTo(ul);
    };


    /* Ajax Cart */
    $('.cart-mini#cart').on('click', function() {
        $('.cart-mini#cart').addClass('active');

        $('.cart-mini#cart').load('index.php?route=module/cart .cart-mini#cart > *');

        $('.cart-mini#cart').on('mouseleave', function() {
            $(this).removeClass('active');
        });
    });

    /* Mega Menu */
    $('#menu ul > li > a + div').each(function(index, element) {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });

    $('.success img, .warning img, .attention img, .information img').on('click', function() {
        $(this).parent().fadeOut('slow', function() {
            $(this).remove();
        });
    });
});

function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

function addToCart(product_id, quantity) {
    quantity = typeof (quantity) !== 'undefined' ? quantity : 1;

    $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: 'product_id=' + product_id + '&quantity=' + quantity,
        dataType: 'json',
        success: function(json) {
            $('.success, .warning, .attention, .information, .error').remove();

            if (json['redirect']) {
                location = json['redirect'];
            }

            if (json['success']) {
                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                $('.success').fadeIn('slow');

                $('#cart-total').html(json['total']);

                $('html, body').animate({scrollTop: 0}, 'slow');
            }
        }
    });
}
function addToWishList(product_id) {
    $.ajax({
        url: 'index.php?route=account/wishlist/add',
        type: 'post',
        data: 'product_id=' + product_id,
        dataType: 'json',
        success: function(json) {
            $('.success, .warning, .attention, .information').remove();

            if (json['success']) {
                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                $('.success').fadeIn('slow');

                $('#wishlist-total').html(json['total']);

                $('html, body').animate({scrollTop: 0}, 'slow');
            }
        }
    });
}

function addToCompare(product_id) {
    $.ajax({
        url: 'index.php?route=product/compare/add',
        type: 'post',
        data: 'product_id=' + product_id,
        dataType: 'json',
        success: function(json) {
            $('.success, .warning, .attention, .information').remove();

            if (json['success']) {
                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                $('.success').fadeIn('slow');

                $('#compare-total, #compare-total-header').html(json['total']);

                $('html, body').animate({scrollTop: 0}, 'slow');
            }
        }
    });
}