<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
    <?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <p><?php echo $text_account_already; ?></p>
      <p><?php echo $text_signup; ?></p><br/>

      <div class="register">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <h5><?php echo $text_your_details; ?></h5>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_firstname; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="form-control" />
                <?php if ($error_firstname) { ?>
                <span class="error"><?php echo $error_firstname; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_lastname; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="form-control" />
                <?php if ($error_lastname) { ?>
                <span class="error"><?php echo $error_lastname; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_email; ?></label>
              <div class="form-control-wrap">
                <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" />
                <?php if ($error_email) { ?>
                <span class="error"><?php echo $error_email; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_telephone; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="form-control" />
                <?php if ($error_telephone) { ?>
                <span class="error"><?php echo $error_telephone; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_fax; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="fax" value="<?php echo $fax; ?>" class="form-control" />
              </div>
            </div>
          </div>

          <h5><?php echo $text_your_address; ?></h5>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><?php echo $entry_company; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="company" value="<?php echo $company; ?>" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_website; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="website" value="<?php echo $website; ?>" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_address_1; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="address_1" value="<?php echo $address_1; ?>" class="form-control" />
                <?php if ($error_address_1) { ?>
                <span class="error"><?php echo $error_address_1; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_address_2; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="address_2" value="<?php echo $address_2; ?>" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_city; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="city" value="<?php echo $city; ?>" class="form-control" />
                <?php if ($error_city) { ?>
                <span class="error"><?php echo $error_city; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span id="postcode-required" class="required" style="display: none">*</span> <?php echo $entry_postcode; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="postcode" value="<?php echo $postcode; ?>" class="form-control" />
                <?php if ($error_postcode) { ?>
                <span class="error"><?php echo $error_postcode; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_country; ?></label>
              <div class="form-control-wrap form-control-wrap-dropdown">
                <select name="country_id" class="form-control" data-url="index.php?route=affiliate/register/country&country_id=" data-zone-id="<?php echo $zone_id; ?>" data-text-select="<?php echo $text_select; ?>" data-text-none="<?php echo $text_none; ?>">
                  <option value="false"><?php echo $text_select; ?></option>
                  <?php foreach ($countries as $country) { ?>
                  <?php if ($country['country_id'] == $country_id) { ?>
                  <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_country) { ?>
                <span class="error"><?php echo $error_country; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_zone; ?></label>
              <div class="form-control-wrap form-control-wrap-dropdown">
                <select class="form-control" name="zone_id" id="zone-list">
                  <option value="0"><?php echo $text_select; ?></option>
                </select>
                <?php if ($error_zone) { ?>
                <span class="error"><?php echo $error_zone; ?></span>
                <?php } ?>
              </div>
            </div>
          </div>

          <h5><?php echo $text_payment; ?></h5>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><?php echo $entry_tax; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="tax" value="<?php echo $tax; ?>" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_payment; ?></label>
              <div class="form-control-wrap">
                <div class="radio-box">
                  <ul class="radio-box-list" id="radio-box-list">
                    <li>
                      <label>
                        <input type="radio" name="payment" value="cheque" <?php if ($payment == 'cheque') { echo 'checked="checked"'; } ?> />
                        <?php echo $text_cheque; ?>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="payment" value="paypal" <?php if ($payment == 'paypal') { echo 'checked="checked"'; } ?> />
                        <?php echo $text_paypal; ?>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input type="radio" name="payment" value="bank" <?php if ($payment == 'bank') { echo 'checked="checked"'; } ?> />
                        <?php echo $text_bank; ?>
                      </label>
                    </li>
                  </ul>
                  <div class="radio-box-expand" id="radio-box-expand">
                    <div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_cheque; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="cheque" value="<?php echo $cheque; ?>" class="form-control" />
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_paypal; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="paypal" value="<?php echo $paypal; ?>" class="form-control" />
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_bank_name; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="bank_name" value="<?php echo $bank_name; ?>" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_bank_branch_number; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_bank_swift_code; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_bank_account_name; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-label"><?php echo $entry_bank_account_number; ?></label>
                        <div class="form-control-wrap">
                          <input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" class="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <h5><?php echo $text_your_password; ?></h5>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_password; ?></label>
              <div class="form-control-wrap">
                <input type="password" name="password" value="<?php echo $password; ?>" class="form-control" />
                <?php if ($error_password) { ?>
                <span class="error"><?php echo $error_password; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_confirm; ?></label>
              <div class="form-control-wrap">
                <input type="password" name="confirm" value="<?php echo $confirm; ?>" class="form-control" />
                <?php if ($error_confirm) { ?>
                <span class="error"><?php echo $error_confirm; ?></span>
                <?php } ?>
              </div>
            </div>
          </div>
          <?php if ($text_agree) { ?>
          <div class="buttons">
            <div class="fr"><?php echo $text_agree; ?>
              <?php if ($agree) { ?>
              <input type="checkbox" name="agree" value="1" checked="checked" />
              <?php } else { ?>
              <input type="checkbox" name="agree" value="1" />
              <?php } ?>
              &nbsp;&nbsp;<button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
            </div>
          </div>
          <?php } else { ?>
          <div class="buttons">
            <div class="fr">
              <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
            </div>
          </div>
          <?php } ?>
        </form>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>