<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    
 <?php echo $column_left; ?>
    
        <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
     
        <?php echo $this->renderPartial('breadcrumbs'); ?>
        
      <h3><?php echo $heading_title; ?></h3>
      <?php echo $text_description; ?>
      <br/>
      <div class="login-content">
        <div class="fl col-half">
          <h5><?php echo $text_new_affiliate; ?></h5>
          <div class="box">
            <?php echo $text_register_account; ?><br/>
            <a href="<?php echo $register; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a>
          </div>
        </div>
        <div class="fr col-half">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <h5><?php echo $text_returning_affiliate; ?></h5>
            <div class="box">
              <p><?php echo $text_i_am_returning_affiliate; ?></p><br/>
              <div class="form-group">
                <label class="form-label"><?php echo $entry_email; ?></label>
                <div class="form-control-wrap">
                  <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="form-label"><?php echo $entry_password; ?></label>
                <div class="form-control-wrap">
                  <input type="password" name="password" value="<?php echo $password; ?>" class="form-control" />
                  <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-inline"><?php echo $button_login; ?></button>
                <?php if ($redirect) { ?>
                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                <?php } ?>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>