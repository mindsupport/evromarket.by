<?php

echo $header; ?>

<main class="wrap">
    <div class="container">

        <?php if ($attention) { ?>
        <div class="attention"><?php echo $attention; ?><img src="catalog/view/theme/default/image/close.png" alt=""
                                                             class="close"/></div>
<?php } ?>
<?php if ($success) { ?>
        <div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close"/>
        </div>
<?php } ?>
<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt=""
                                                               class="close"/></div>
<?php } ?>


 <?php echo $column_left; ?>
        <?php echo $column_right; ?>
        <div class="content"><?php echo $content_top; ?>
         <?php echo $this->renderPartial('breadcrumbs'); ?>

            <div class="cart">
                <h3><?php echo $heading_title; ?>
                    <!--<?php if ($weight) { ?>
                    &nbsp;(<?php echo $weight; ?>)
                    <?php } ?>-->
                </h3>

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <table class="table-bordered">
                        <thead>
                            <tr>
                                <th class="col-image"><?php echo $column_image; ?></th>
                                <th class="col-title"><?php echo $column_name; ?></th>
                                <th class="col-model"><?php echo $column_model; ?></th>
                                <th class="col-amount"><?php echo $column_quantity; ?></th>
                                <th class="col-price"><?php echo $column_price; ?></th>
                                <th class="col-total"><?php echo $column_total; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $product) { ?>
                        <?php if($product['recurring']): ?>
                            <tr>
                                <td colspan="6" style="border:none;">
                                    <image src="catalog/view/theme/default/image/reorder.png" alt="" title=""
                                           style="float:left;"/><span style="float:left;line-height:18px; margin-left:10px;">
                                        <strong><?php echo $text_recurring_item ?></strong>
                                    <?php echo $product['profile_description'] ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                            <tr>
                                <td class="col-image"><?php if ($product['thumb']) { ?>
                                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                                   alt="<?php echo $product['name']; ?>"
                                                                                   title="<?php echo $product['name']; ?>"/></a>
                                <?php } ?></td>
                                <td class="col-title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                <?php if (!$product['stock']) { ?>
                                    <span class="stock">***</span>
                                <?php } ?>
                                    <div>
                                    <?php foreach ($product['option'] as $option) { ?>
                                        -
                                        <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <br/>
                                    <?php } ?>
                                    <?php if($product['recurring']): ?>
                                        -
                                        <small><?php echo $text_payment_profile ?>: <?php echo $product['profile_name'] ?></small>
                                    <?php endif; ?>
                                    </div>
                                <?php if ($product['reward']) { ?>
                                    <small><?php echo $product['reward']; ?></small>
                                <?php } ?></td>
                                <td class="col-model">
                                    <p><?php echo $product['model']; ?></p>
                                </td>
                                <td class="col-amount">
                                    <input type="text" class="amount-field" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1"/>
                                    <button type="submit" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" class="refresh"><i class="icon icon-refresh"></i></button>
                                </td>
                                <td class="col-price">
                                    <p class="price"><?php echo $product['price']; ?></p>
                                </td>
                                <td class="col-total">
                                    <a href="<?php echo $product['remove']; ?>" class="remove">x</a>
                                    <p class="price"><?php echo $product['total']; ?></p>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php foreach ($vouchers as $vouchers) { ?>
                            <tr>
                                <td class="image"></td>
                                <td class="name"><?php echo $vouchers['description']; ?></td>
                                <td class="model"></td>
                                <td class="quantity"><input type="text" name="" value="1" size="1" disabled="disabled"/>
                                    &nbsp;<a href="<?php echo $vouchers['remove']; ?>"><img
                                            src="catalog/view/theme/default/image/remove.png"
                                            alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>"/></a></td>
                                <td class="price"><?php echo $vouchers['amount']; ?></td>
                                <td class="total"><?php echo $vouchers['amount']; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>

                <?php if ($coupon_status || $voucher_status || $reward_status || $shipping_status) { ?>
                <div class="radio-box">
                    <h6><?php echo $text_next; ?></h6>

                    <ul class="radio-box-list" id="radio-box-list">
                        <li>
                            <label>
                                <input type="radio" name="next" value="coupon" <?php if ($next == 'coupon') { echo 'checked="checked"'; }?> />
                                <?php echo $text_use_coupon; ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="next" value="voucher" <?php if ($next == 'voucher') { echo 'checked="checked"'; } ?> />
                                <?php echo $text_use_voucher; ?>
                            </label>
                        </li>
                        <?php if ($reward_status) { ?>
                        <li>
                            <label>
                                <input type="radio" name="next" value="reward" <?php if ($next == 'reward') { echo 'checked="checked"'; } ?> />
                                <?php echo $text_use_reward; ?>
                            </label>
                        </li>
                        <?php } ?>
                        <?php if ($shipping_status) { ?>
                        <li>
                            <label>
                                <input type="radio" name="next" value="shipping" <?php if ($next == 'shipping') { echo 'checked="checked"'; } ?> />
                                <?php echo $text_shipping_estimate; ?>
                            </label>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="radio-box-expand" id="radio-box-expand">
                        <div>
                            <h6><?php echo $entry_coupon; ?></h6>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="form-label"><?php echo $entry_coupon_label; ?></label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="coupon" value="<?php echo $coupon; ?>" />
                                        <input type="hidden" name="next" value="coupon"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">&nbsp;</label>
                                    <button type="submit" class="btn"><?php echo $button_coupon; ?></button>
                                </div>
                            </form>
                        </div>
                        <div>
                            <h6><?php echo $entry_voucher; ?></h6>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="form-label">Номер сертификата:</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="voucher" value="<?php echo $voucher; ?>" />
                                        <input type="hidden" name="next" value="voucher"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">&nbsp;</label>
                                    <button type="submit" class="btn"><?php echo $button_voucher; ?></button>
                                </div>
                            </form>
                        </div>
                        <?php if ($reward_status) { ?>
                        <div>
                            <h6><?php echo $entry_reward; ?></h6>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="reward" value="<?php echo $reward; ?>"/>
                                    <input type="hidden" name="next" value="reward"/>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn"><?php echo $button_reward; ?></button>
                                </div>
                            </form>
                        </div>
                        <?php } ?>
                        <?php if ($shipping_status) { ?>
                        <div>
                            <h6><?php echo $text_shipping_detail; ?></h6>
                            <div class="form-group">
                                <label class="form-label"><?php echo $entry_country; ?></label>
                                <div class="form-control-wrap">
                                    <select class="form-control" name="country_id" data-url="index.php?route=checkout/cart/country&country_id=" data-zone-id="<?php echo $zone_id; ?>" data-text-select="<?php echo $text_select; ?>" data-text-none="<?php echo $text_none; ?>">
                                        <option value="0"><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $country) { ?>
                                        <?php if ($country['country_id'] == $country_id) { ?>
                                        <option value="<?php echo $country['country_id']; ?>"
                                                selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="region"><?php echo $entry_zone; ?></label>
                                <div class="form-control-wrap">
                                    <select class="form-control" name="zone_id" id="zone-list">
                                        <option value="0"><?php echo $text_select; ?></option>
                                    </select>
                                    <!--<input type="text" id="region" class="form-control" name="zone_id" />-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="zip"><?php echo $entry_postcode; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" id="zip" class="form-control" name="postcode" value="<?php echo $postcode; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">&nbsp;</label>
                                <button type="button" id="button-quote" class="btn"><?php echo $button_quote; ?></button>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <!--<div class="cart-module">
                        <div id="reward" class="content" style="display: <?php echo ($next == 'reward' ? 'block' : 'none'); ?>;">
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <?php echo $entry_reward; ?>&nbsp;
                                <input type="text" name="reward" value="<?php echo $reward; ?>"/>
                                <input type="hidden" name="next" value="reward"/>
                                &nbsp;
                                <input type="submit" value="<?php echo $button_reward; ?>" class="button"/>
                            </form>
                        </div>
                        <div id="shipping" class="content" style="display: <?php echo ($next == 'shipping' ? 'block' : 'none'); ?>;">
                            <p><?php echo $text_shipping_detail; ?></p>
                            <table>
                                <tr>
                                    <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                                    <td><select name="country_id">
                                            <option value=""><?php echo $text_select; ?></option>
                                            <?php foreach ($countries as $country) { ?>
                                            <?php if ($country['country_id'] == $country_id) { ?>
                                            <option value="<?php echo $country['country_id']; ?>"
                                                    selected="selected"><?php echo $country['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                                    <td><select name="zone_id">
                                        </select></td>
                                </tr>
                                <tr>
                                    <td><span id="postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td>
                                    <td><input type="text" name="postcode" value="<?php echo $postcode; ?>"/></td>
                                </tr>
                            </table>
                            <input type="button" value="<?php echo $button_quote; ?>" id="button-quote" class="button"/>
                        </div>
                    </div>
                </div>-->
                </div>
                <?php } ?>
                <div class="cart-total">
                    <div class="fr">
                        <a href="<?php echo $checkout; ?>" class="add-to-cart btn btn-side"><?php echo $button_checkout; ?><i class="icon icon-cart"></i></a>
                    </div>
                    <!--<div class="fr">
                      <a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_shopping; ?></a>
                    </div>-->
                    <p><?php echo $totals[1]['title']; ?>: &nbsp;&nbsp; <?php echo $totals[1]['text']; ?></p>
                </div>
            </div>
            <!--<?php echo $content_bottom; ?>-->
        </div>

        <?php if ($shipping_status) { ?>
        <script type="text/javascript"><!--
            $('#button-quote').on('click', function() {
                $.ajax({
                    url: 'index.php?route=checkout/cart/quote',
                    type: 'post',
                    data: 'country_id=' + $('[name=country_id]').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#button-quote').attr('disabled', true);
                        $('#button-quote').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                    },
                    complete: function() {
                        $('#button-quote').attr('disabled', false);
                        $('.wait').remove();
                    },
                    success: function(json) {
                        $('.success, .warning, .attention, .error').remove();

                        if (json['error']) {
                            if (json['error']['warning']) {
                                $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                                $('.warning').fadeIn('slow');

                                $('html, body').animate({scrollTop: 0}, 'slow');
                            }

                            if (json['error']['country']) {
                                $('[name=country_id]').after('<span class="error">' + json['error']['country'] + '</span>');
                            }

                            if (json['error']['zone']) {
                                $('[name=zone_id]').after('<span class="error">' + json['error']['zone'] + '</span>');
                            }

                            if (json['error']['postcode']) {
                                $('input[name=postcode]').after('<span class="error">' + json['error']['postcode'] + '</span>');
                            }
                        }

                        if (json['shipping_method']) {
                            html = '<h2><?php echo $text_shipping_method; ?></h2>';
                            html += '<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">';
                            html += '  <table class="radio">';

                            for (i in json['shipping_method']) {
                                html += '<tr>';
                                html += '  <td colspan="3"><b>' + json['shipping_method'][i]['title'] + '</b></td>';
                                html += '</tr>';

                                if (!json['shipping_method'][i]['error']) {
                                    for (j in json['shipping_method'][i]['quote']) {
                                        html += '<tr class="highlight">';

                                        if (json['shipping_method'][i]['quote'][j]['code'] == '<?php echo $shipping_method; ?>') {
                                            html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" /></td>';
                                        } else {
                                            html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" /></td>';
                                        }

                                        html += '  <td><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['title'] + '</label></td>';
                                        html += '  <td style="text-align: right;"><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['text'] + '</label></td>';
                                        html += '</tr>';
                                    }
                                } else {
                                    html += '<tr>';
                                    html += '  <td colspan="3"><div class="error">' + json['shipping_method'][i]['error'] + '</div></td>';
                                    html += '</tr>';
                                }
                            }

                            html += '  </table>';
                            html += '  <br />';
                            html += '  <input type="hidden" name="next" value="shipping" />';

                        <?php
                            if ($shipping_method) {
                        ?>
                            html += '  <input type="submit" value="<?php echo $button_shipping; ?>" id="button-shipping" class="button" />';
                        <?php } else { ?>
                            html += '  <input type="submit" value="<?php echo $button_shipping; ?>" id="button-shipping" class="button" disabled="disabled" />';
                        <?php } ?>

                            html += '</form>';

                            $.colorbox({
                                overlayClose: true,
                                opacity: 0.5,
                                width: '600px',
                                height: '400px',
                                href: false,
                                html: html
                            });

                            $('input[name=\'shipping_method\']').bind('change', function() {
                                $('#button-shipping').attr('disabled', false);
                            });
                        }
                    }
                });
            });
            //--></script>
        <?php } ?>
    </div>
</main>
<?php echo $footer; ?>
