<?php echo $header; ?>

<main class="wrap">
    <div class="container">
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>
        <div class="content"><?php echo $content_top; ?>
            
            <?php echo $this->renderPartial('breadcrumbs'); ?>
            
            <h3><?php echo $heading_title; ?></h3>
            <br/>
            <br/>
            <p><?php echo $text_error; ?></p>
        </div>
    </div>
</main>
<?php echo $footer; ?>