<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    
        <?php echo $column_left; ?>
      
      <?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <div class="account">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="box">
            <div class="form-group">
              <label class="form-label"><?php echo $entry_newsletter; ?></label>
              <div class="form-control-wrap form-control-wrap-radio">
                <?php if ($newsletter) { ?>
                  <input type="radio" name="newsletter" value="1" checked="checked" />
                  <?php echo $text_yes; ?>&nbsp;
                  <input type="radio" name="newsletter" value="0" />
                  <?php echo $text_no; ?>
                  <?php } else { ?>
                  <input type="radio" name="newsletter" value="1" />
                  <?php echo $text_yes; ?>&nbsp;
                  <input type="radio" name="newsletter" value="0" checked="checked" />
                  <?php echo $text_no; ?>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="buttons">
            <div class="fl">
              <a href="<?php echo $back; ?>" class="btn btn-inline"><?php echo $button_back; ?></a>
            </div>
            <div class="fr">
              <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>