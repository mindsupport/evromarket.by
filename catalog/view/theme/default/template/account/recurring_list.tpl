<?php echo $header; ?>
<main class="wrap">
  <div class="container">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <?php if ($profiles) { ?>
        <table class="list table-bordered">
            <thead>
            <tr>
                <th class="left"><?php echo $column_profile_id; ?></th>
                <th class="left"><?php echo $column_created; ?></th>
                <th class="left"><?php echo $column_status; ?></th>
                <th class="left"><?php echo $column_product; ?></th>
                <th class="right"><?php echo $column_action; ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if ($profiles) { ?>
            <?php foreach ($profiles as $profile) { ?>
            <tr>
                <td class="left">#<?php echo $profile['id']; ?></td>
                <td class="left"><?php echo $profile['created']; ?></td>
                <td class="left"><?php echo $status_types[$profile['status']]; ?></td>
                <td class="left"><?php echo $profile['name']; ?></td>
                <td class="right"><a href="<?php echo $profile['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
                <td class="center" colspan="5"><?php echo $text_empty; ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

      <div class="pagination"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <div class="empty"><?php echo $text_empty; ?></div>
      <?php } ?>
      <div class="buttons">
        <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>