<?php

echo $header; ?>

<main class="wrap">
    <div class="container">

    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>

        <?php echo $column_left; ?>

        <?php echo $column_right; ?>

        <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>
            <h3><?php echo $heading_title; ?></h3>

            <div class="login-page cf">
                <div class="fl">
                    <h5><?php echo $text_new_customer; ?></h5>

                    <div class="box">
                        <p><?php echo $text_register; ?></p>
                        <br/>
                        <p><?php echo $text_register_account; ?></p>
                        <br/>
                        <a href="<?php echo $register; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a>
                    </div>
                </div>
                <div class="fr">
                    <h5><?php echo $text_returning_customer; ?></h5>

                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <div class="box">
                            <p><?php echo $text_i_am_returning_customer; ?></p>
                            <br/>
                            <div class="form-group">
                                <label><?php echo $entry_email; ?></label>
                                <div class="form-control-wrap">
                                    <input type="email" class="form-control" name="email" value="<?php echo $email; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_password; ?></label>
                                <div class="form-control-wrap">
                                    <input type="password" class="form-control" name="password" value="<?php echo $password; ?>"/>
                                    <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a><br/>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-inline"><?php echo $button_login; ?></button>
              <?php if ($redirect) { ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
              <?php } ?>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <script type="text/javascript"><!--
          $('#login input').keydown(function(e) {
                if (e.keyCode == 13) {
                    $('#login').submit();
                }
            });
            //--></script>
    </div>
</main>
<?php echo $footer; ?>