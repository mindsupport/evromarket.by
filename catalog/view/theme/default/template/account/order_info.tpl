<?php

echo $header; ?>
<main class="wrap">
    <div class="container">

 <?php echo $column_left; ?>

      <?php echo $column_right; ?>

        <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

            <h3><?php echo $heading_title; ?></h3>

            <!--<?php echo $text_order_detail; ?>-->

            <div class="order-list">
                <div class="order-content">
                    <div> 
                        <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?>
                    </div>

                    <?php if ($invoice_no) { ?>
                    <div>   
                        <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?>
                    </div>
              <?php } ?>


                    <div> <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></div>

                    <?php if ($payment_method) { ?>
                    <div>
                        <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
                    </div>
              <?php } ?>

            <?php if ($shipping_method) { ?>
                    <div><b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?></div>
              <?php } ?>
                </div>
            </div>

            <div class="order-list">
                <div class="order-content">
                    <b><?php echo $text_payment_address; ?></b>
                    <br>
                    <?php echo $payment_address; ?>
                </div>
                <div>
                     <?php if ($shipping_address) { ?>
                    <b><?php echo $text_shipping_address; ?></b>
                    <br>
                    <?php echo $shipping_address; ?>
                    <?php } ?>
                </div>
            </div>
            
            <table class="list">
                <thead>
                    <tr>
                        <td class="left"><b><?php echo $column_name; ?></b></td>
                        <td class="left"><b><?php echo $column_model; ?></b></td>
                        <td class="right"><b><?php echo $column_quantity; ?></b></td>
                        <td class="right"><b><?php echo $column_price; ?></b></td>
                        <td class="right"><b><?php echo $column_total; ?></b></td>
            <?php if ($products) { ?>
                        <td style="width: 1px;"></td>
            <?php } ?>
                    </tr>
                </thead>
                <tbody>
          <?php foreach ($products as $product) { ?>
                    <tr>
                        <td class="left"><?php echo $product['name']; ?>
              <?php foreach ($product['option'] as $option) { ?>
                            <br />
                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
              <?php } ?></td>
                        <td class="left"><?php echo $product['model']; ?></td>
                        <td class="right"><?php echo $product['quantity']; ?></td>
                        <td class="right"><?php echo $product['price']; ?></td>
                        <td class="right"><?php echo $product['total']; ?></td>
                        <td class="right"><a href="<?php echo $product['return']; ?>"><?php echo $button_return; ?></a></td>
                    </tr>
          <?php } ?>
          <?php foreach ($vouchers as $voucher) { ?>
                    <tr>
                        <td class="left"><?php echo $voucher['description']; ?></td>
                        <td class="left"></td>
                        <td class="right">1</td>
                        <td class="right"><?php echo $voucher['amount']; ?></td>
                        <td class="right"><?php echo $voucher['amount']; ?></td>
            <?php if ($products) { ?>
                        <td></td>
            <?php } ?>
                    </tr>
          <?php } ?>
                </tbody>
                <tfoot>
          <?php foreach ($totals as $total) { ?>
                    <tr>
                        <td colspan="3"></td>
                        <td class="right"><b><?php echo $total['title']; ?>:</b></td>
                        <td class="right"><?php echo $total['text']; ?></td>
            <?php if ($products) { ?>
                        <td></td>
            <?php } ?>
                    </tr>
          <?php } ?>
                </tfoot>
            </table>
      <?php if ($comment) { ?>
            <table class="list">
                <thead>
                    <tr>
                        <td class="left"><?php echo $text_comment; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="left"><?php echo $comment; ?></td>
                    </tr>
                </tbody>
            </table>
      <?php } ?>

      <?php if ($histories) { ?>
            <br>
            <h3><?php echo $text_history; ?></h3>
       <?php foreach ($histories as $history) { ?>
            <div class="order-list">
                <div class="order-id"><b><?php echo $column_date_added; ?></b> #<?php echo $history['date_added']; ?></div>        
                <div class="order-content">
                    <div><b><?php echo $column_status; ?>:</b> <?php echo $history['status']; ?></div>
             <?php if(!empty($history['comment'])): ?>
                    <div>
                <?php echo $column_comment; ?>: <?php echo $history['comment']; ?>
                    </div>
             <?php endif;?>
                </div>

            </div>
      <?php } ?>
      <?php } ?>

            <div class="buttons">
                <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
            </div>
        </div>
    </div>
</main>
<?php echo $footer; ?> 