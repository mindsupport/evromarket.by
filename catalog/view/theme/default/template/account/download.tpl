<?php

echo $header; ?>
<main class="wrap">
    <div class="container">

        <?php echo $column_left; ?>
      <?php echo $column_right; ?>

        <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

            <h3><?php echo $heading_title; ?></h3>

      <?php foreach ($downloads as $download) { ?>
            <div class="download-list">
                <div class="download-id"><b><?php echo $text_order; ?></b> <?php echo $download['order_id']; ?></div>
                <div class="download-status"><b><?php echo $text_size; ?></b> <?php echo $download['size']; ?></div>
                <div class="download-content">
                    <div><b><?php echo $text_name; ?></b> <?php echo $download['name']; ?><br />
                        <b><?php echo $text_date_added; ?></b> <?php echo $download['date_added']; ?></div>
                    <div><b><?php echo $text_remaining; ?></b> <?php echo $download['remaining']; ?></div>
                    <div class="download-info">
            <?php if ($download['remaining'] > 0) { ?>
                        <a href="<?php echo $download['href']; ?>"><img src="catalog/view/theme/default/image/download.png" alt="<?php echo $button_download; ?>" title="<?php echo $button_download; ?>" /></a>
            <?php } ?>
                    </div>
                </div>
            </div>
      <?php } ?>
            <div class="pagination"><?php echo $pagination; ?></div>
            <div class="buttons">
                <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
            </div>
        </div>
    </div>
</main>
<?php echo $footer; ?>