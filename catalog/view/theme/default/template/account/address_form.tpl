<?php

echo $header; ?>
<main class="wrap">
    <div class="container">
        <?php echo $column_left; ?>

        <?php echo $column_right; ?>

        <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

            <h3><?php echo $heading_title; ?></h3>

            <div class="account">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <h5><?php echo $text_edit_address; ?></h5>
                    <div class="box">
                        <div class="form-group">
                            <label class="form-label"><span class="required">*</span> <?php echo $entry_firstname; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="form-control" />
                <?php if ($error_firstname) { ?>
                                <span class="error"><?php echo $error_firstname; ?></span>
                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><span class="required">*</span> <?php echo $entry_lastname; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="form-control" />
                <?php if ($error_lastname) { ?>
                                <span class="error"><?php echo $error_lastname; ?></span>
                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><?php echo $entry_company; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="company" value="<?php echo $company; ?>" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><span class="required">*</span> <?php echo $entry_address_1; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="address_1" value="<?php echo $address_1; ?>" class="form-control" />
                <?php if ($error_address_1) { ?>
                                <span class="error"><?php echo $error_address_1; ?></span>
                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><?php echo $entry_address_2; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="address_2" value="<?php echo $address_2; ?>" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><span class="required">*</span> <?php echo $entry_city; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="city" value="<?php echo $city; ?>" class="form-control" />
                <?php if ($error_city) { ?>
                                <span class="error"><?php echo $error_city; ?></span>
                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><span id="postcode-required" class="required" style="display: none">*</span> <?php echo $entry_postcode; ?></label>
                            <div class="form-control-wrap">
                                <input type="text" name="postcode" value="<?php echo $postcode; ?>" class="form-control" />
                <?php if ($error_postcode) { ?>
                                <span class="error"><?php echo $error_postcode; ?></span>
                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><span class="required">*</span> <?php echo $entry_country; ?></label>
                            <div class="form-control-wrap form-control-wrap-dropdown">
                                <select name="country_id" class="form-control" data-url="index.php?route=account/address/country&country_id=" data-zone-id="<?php echo $zone_id; ?>" data-text-select="<?php echo $text_select; ?>" data-text-none="<?php echo $text_none; ?>">
                                    <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($countries as $country) { ?>
                  <?php if ($country['country_id'] == $country_id) { ?>
                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                  <?php } else { ?>
                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                                </select>
                <?php if ($error_country) { ?>
                                <span class="error"><?php echo $error_country; ?></span>
                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><span class="required">*</span> <?php echo $entry_zone; ?></label>
                            <div class="form-control-wrap form-control-wrap-dropdown">
                                <select name="zone_id" class="form-control" id="zone-list">
                                    <option value="0"><?php echo $text_select; ?></option>
                                </select>
                <?php if ($error_zone) { ?>
                                <span class="error"><?php echo $error_zone; ?></span>
                <?php } ?></div>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><?php echo $entry_default; ?></label>
                            <div class="form-control-wrap form-control-wrap-radio">
                <?php if ($default) { ?>
                                <input type="radio" name="default" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                                <input type="radio" name="default" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                                <input type="radio" name="default" value="1" />
                <?php echo $text_yes; ?>
                                <input type="radio" name="default" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="buttons">
                        <div class="fl">
                            <a href="<?php echo $back; ?>" class="btn btn-inline"><?php echo $button_back; ?></a>
                        </div>
                        <div class="fr">
                            <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<?php echo $footer; ?>