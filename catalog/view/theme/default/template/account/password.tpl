<?php echo $header; ?>
<main class="wrap">
  <div class="container">
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>
      <div class="account">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <h5><?php echo $text_password; ?></h5>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_password; ?></label>
              <div class="form-control-wrap">
                <input type="password" name="password" value="<?php echo $password; ?>" class="form-control" />
                <?php if ($error_password) { ?>
                <span class="error"><?php echo $error_password; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_confirm; ?></label>
              <div class="form-control-wrap">
                <input type="password" name="confirm" value="<?php echo $confirm; ?>" class="form-control" />
                <?php if ($error_confirm) { ?>
                <span class="error"><?php echo $error_confirm; ?></span>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="buttons">
            <div class="fl">
              <a href="<?php echo $back; ?>" class="btn btn-inline"><?php echo $button_back; ?></a>
            </div>
            <div class="fr">
              <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>