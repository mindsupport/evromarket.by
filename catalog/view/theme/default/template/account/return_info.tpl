<?php echo $header; ?>
<main class="wrap">
  <div class="container">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <table class="list table-bordered">
        <thead>
          <tr>
            <th class="left" colspan="2"><?php echo $text_return_detail; ?></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="left" style="width: 50%;"><b><?php echo $text_return_id; ?></b> #<?php echo $return_id; ?><br />
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
            <td class="left" style="width: 50%;"><b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
              <b><?php echo $text_date_ordered; ?></b> <?php echo $date_ordered; ?></td>
          </tr>
        </tbody>
      </table>

      <h5><?php echo $text_product; ?></h5>

      <table class="list table-bordered">
        <thead>
          <tr>
            <th class="left" style="width: 33.3%;"><?php echo $column_product; ?></th>
            <th class="left" style="width: 33.3%;"><?php echo $column_model; ?></th>
            <th class="right" style="width: 33.3%;"><?php echo $column_quantity; ?></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="left"><?php echo $product; ?></td>
            <td class="left"><?php echo $model; ?></td>
            <td class="right"><?php echo $quantity; ?></td>
          </tr>
        </tbody>
      </table>
      <table class="list table-bordered">
        <thead>
          <tr>
            <th class="left" style="width: 33.3%;"><?php echo $column_reason; ?></th>
            <th class="left" style="width: 33.3%;"><?php echo $column_opened; ?></th>
            <th class="left" style="width: 33.3%;"><?php echo $column_action; ?></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="left"><?php echo $reason; ?></td>
            <td class="left"><?php echo $opened; ?></td>
            <td class="left"><?php echo $action; ?></td>
          </tr>
        </tbody>
      </table>
      <?php if ($comment) { ?>
      <table class="list table-bordered">
        <thead>
          <tr>
            <th class="left"><?php echo $text_comment; ?></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>

      <?php if ($histories) { ?>
      <h5><?php echo $text_history; ?></h5>
      <table class="list table-bordered">
        <thead>
          <tr>
            <th class="left" style="width: 33.3%;"><?php echo $column_date_added; ?></th>
            <th class="left" style="width: 33.3%;"><?php echo $column_status; ?></th>
            <th class="left" style="width: 33.3%;"><?php echo $column_comment; ?></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="left"><?php echo $history['date_added']; ?></td>
            <td class="left"><?php echo $history['status']; ?></td>
            <td class="left"><?php echo $history['comment']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } ?>
      <div class="buttons">
        <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>