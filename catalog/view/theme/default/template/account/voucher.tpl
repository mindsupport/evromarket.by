<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
      <?php echo $this->renderPartial('breadcrumbs'); ?>

      <div class="account">
        <h3><?php echo $heading_title; ?></h3>
        <p><?php echo $text_description; ?></p><br/>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="box">
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_to_name; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="to_name" value="<?php echo $to_name; ?>" class="form-control" />
                <?php if ($error_to_name) { ?>
                <span class="error"><?php echo $error_to_name; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_to_email; ?></label>
              <div class="form-control-wrap">
                <input type="email" name="to_email" value="<?php echo $to_email; ?>" class="form-control" />
                <?php if ($error_to_email) { ?>
                <span class="error"><?php echo $error_to_email; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_from_name; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="from_name" value="<?php echo $from_name; ?>" class="form-control" />
                <?php if ($error_from_name) { ?>
                <span class="error"><?php echo $error_from_name; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_from_email; ?></label>
              <div class="form-control-wrap">
                <input type="email" name="from_email" value="<?php echo $from_email; ?>" class="form-control" />
                <?php if ($error_from_email) { ?>
                <span class="error"><?php echo $error_from_email; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_theme; ?></label>
              <div class="form-control-wrap form-control-wrap-radio">
                <?php foreach ($voucher_themes as $voucher_theme) { ?>
                  <?php if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
                  <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>"
                         id="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>" checked="checked"/>
                  <label
                    for="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"><?php echo $voucher_theme['name']; ?></label>
                  <?php } else { ?>
                  <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>"
                         id="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"/>
                  <label
                    for="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"><?php echo $voucher_theme['name']; ?></label>
                  <?php } ?>
                  <br/>
                  <?php } ?>
                  <?php if ($error_theme) { ?>
                  <span class="error"><?php echo $error_theme; ?></span>
                  <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_message; ?></label>
              <div class="form-control-wrap">
                <textarea name="message" class="form-control"><?php echo $message; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_amount; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="amount" value="<?php echo $amount; ?>" class="form-control" />
                <?php if ($error_amount) { ?>
                <span class="error"><?php echo $error_amount; ?></span>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="buttons">
            <div class="fr"><?php echo $text_agree; ?>
              <?php if ($agree) { ?>
              <input type="checkbox" name="agree" value="1" checked="checked"/>
              <?php } else { ?>
              <input type="checkbox" name="agree" value="1"/>
              <?php } ?>
              &nbsp;&nbsp;<button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>