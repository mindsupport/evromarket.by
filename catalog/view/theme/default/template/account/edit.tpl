<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php echo $column_left; ?><?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <div class="account">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <h5><?php echo $text_your_details; ?></h5>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_firstname; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="form-control" />
                <?php if ($error_firstname) { ?>
                <span class="error"><?php echo $error_firstname; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_lastname; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="form-control" />
                <?php if ($error_lastname) { ?>
                <span class="error"><?php echo $error_lastname; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_email; ?></label>
              <div class="form-control-wrap">
                <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" />
                <?php if ($error_email) { ?>
                <span class="error"><?php echo $error_email; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><span class="required">*</span> <?php echo $entry_telephone; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="form-control" />
                <?php if ($error_telephone) { ?>
                <span class="error"><?php echo $error_telephone; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_fax; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="fax" value="<?php echo $fax; ?>" class="form-control" />
              </div>
            </div>
          </div>
          <div class="buttons">
            <div class="fl">
              <a href="<?php echo $back; ?>" class="btn btn-inline"><?php echo $button_back; ?></a>
            </div>
            <div class="fr">
              <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>