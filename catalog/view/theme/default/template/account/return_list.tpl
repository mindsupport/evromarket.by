<?php echo $header; ?>

<main class="wrap">
  <div class="container">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <?php if ($returns) { ?>
      <?php foreach ($returns as $return) { ?>
      <div class="return-list">
        <div class="return-id"><b><?php echo $text_return_id; ?></b> #<?php echo $return['return_id']; ?></div>
        <div class="return-status"><b><?php echo $text_status; ?></b> <?php echo $return['status']; ?></div>
        <div class="return-content">
          <div><b><?php echo $text_date_added; ?></b> <?php echo $return['date_added']; ?><br />
            <b><?php echo $text_order_id; ?></b> <?php echo $return['order_id']; ?></div>
          <div><b><?php echo $text_customer; ?></b> <?php echo $return['name']; ?></div>
          <div class="return-info"><a href="<?php echo $return['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a></div>
        </div>
      </div>
      <?php } ?>
      <div class="pagination"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <div class="empty"><?php echo $text_empty; ?></div>
      <?php } ?>
      <div class="buttons">
        <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
      </div>
    </div>
  </div>
</main>

<?php echo $footer; ?>