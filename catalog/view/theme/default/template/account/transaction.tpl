<?php

echo $header; ?>
<main class="wrap">
    <div class="container">
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>

        <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

            <h3><?php echo $heading_title; ?></h3>

            <p><?php echo $text_total; ?><strong> <?php echo $total; ?></strong>.</p><br/>

            <table class="list table-bordered">
                <thead>
                    <tr>
                        <th class="left"><?php echo $column_date_added; ?></th>
                        <th class="left"><?php echo $column_description; ?></th>
                        <th class="right"><?php echo $column_amount; ?></th>
                    </tr>
                </thead>
                <tbody>
          <?php if ($transactions) { ?>
          <?php foreach ($transactions  as $transaction) { ?>
                    <tr>
                        <td class="left"><?php echo $transaction['date_added']; ?></td>
                        <td class="left"><?php echo $transaction['description']; ?></td>
                        <td class="right"><?php echo $transaction['amount']; ?></td>
                    </tr>
          <?php } ?>
          <?php } else { ?>
                    <tr>
                        <td class="center" colspan="5"><?php echo $text_empty; ?></td>
                    </tr>
          <?php } ?>
                </tbody>
            </table>
      <?php if($pagination) { ?>
            <div class="pagination"><?php echo $pagination; ?></div>
      <?php } ?>
            <div class="buttons">
                <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
            </div>
        </div>
    </div>
</main>
<?php echo $footer; ?>