<?php echo $header; ?>
<main class="wrap">
  <div class="container">
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
      <?php echo $column_left; ?>
        <?php echo $column_right; ?>
  <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>
    <h3><?php echo $heading_title; ?></h3>

    <p><?php echo $text_account_already; ?></p>

    <div class="register">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <h5><?php echo $text_your_details; ?></h5>
        <div class="box">
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_firstname; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="firstname" value="<?php echo $firstname; ?>"/>
              <?php if ($error_firstname) { ?>
              <span class="error"><?php echo $error_firstname; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_lastname; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="lastname" value="<?php echo $lastname; ?>"/>
              <?php if ($error_lastname) { ?>
              <span class="error"><?php echo $error_lastname; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_email; ?></label>
            <div class="form-control-wrap">
              <input type="email" class="form-control" name="email" value="<?php echo $email; ?>"/>
              <?php if ($error_email) { ?>
              <span class="error"><?php echo $error_email; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_telephone; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="telephone" value="<?php echo $telephone; ?>"/>
              <?php if ($error_telephone) { ?>
              <span class="error"><?php echo $error_telephone; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><?php echo $entry_fax; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="fax" value="<?php echo $fax; ?>"/>
            </div>
          </div>
        </div>

        <h5><?php echo $text_your_address; ?></h5>
        <div class="box">
          <div class="form-group">
            <label class="form-label"><?php echo $entry_company; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="company" value="<?php echo $company; ?>"/>
            </div>
          </div>
          <?php if(count($customer_groups) > 1) { ?>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_customer_group; ?></label>
              <div class="form-control-wrap">
                <?php foreach ($customer_groups as $customer_group) { ?>
                  <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                  <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>"
                         id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked"/>
                  <label
                    for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
                  <br/>
                  <?php } else { ?>
                  <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>"
                         id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"/>
                  <label
                    for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
                  <br/>
                  <?php } ?>
                  <?php } ?>
                <div id="company-id-display" class="form-group">
                  <label class="form-label"><span id="company-id-required" class="required">*</span> <?php echo $entry_company_id; ?></label>
                  <div class="form-control-wrap">
                    <input type="text" class="form-control" name="company_id" value="<?php echo $company_id; ?>" />
                    <?php if ($error_company_id) { ?>
                    <span class="error"><?php echo $error_company_id; ?></span>
                    <?php } ?>
                  </div>
                </div>
                <div id="tax-id-display" class="form-group">
                  <label class="form-label"><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?></label>
                  <div class="form-control-wrap">
                    <input type="text" class="form-control" name="tax_id" value="<?php echo $tax_id; ?>"/>
                    <?php if ($error_tax_id) { ?>
                    <span class="error"><?php echo $error_tax_id; ?></span>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_address_1; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="address_1" value="<?php echo $address_1; ?>"/>
              <?php if ($error_address_1) { ?>
              <span class="error"><?php echo $error_address_1; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><?php echo $entry_address_2; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="address_2" value="<?php echo $address_2; ?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_city; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="city" value="<?php echo $city; ?>"/>
              <?php if ($error_city) { ?>
              <span class="error"><?php echo $error_city; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_postcode; ?></label>
            <div class="form-control-wrap">
              <input type="text" class="form-control" name="postcode" value="<?php echo $postcode; ?>"/>
              <?php if ($error_postcode) { ?>
              <span class="error"><?php echo $error_postcode; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_country; ?></label>
            <div class="form-control-wrap form-control-wrap-dropdown">
              <select name="country_id" class="form-control" data-url="index.php?route=account/register/country&country_id=" data-zone-id="<?php echo $zone_id; ?>" data-text-select="<?php echo $text_select; ?>" data-text-none="<?php echo $text_none; ?>">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                <option value="<?php echo $country['country_id']; ?>"
                        selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_postcode) { ?>
              <span class="error"><?php echo $error_postcode; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_zone; ?></label>
            <div class="form-control-wrap form-control-wrap-dropdown">
              <select class="form-control" name="zone_id" id="zone-list">
                <option value="0"><?php echo $text_select; ?></option>
              </select>
              <?php if ($error_zone) { ?>
              <span class="error"><?php echo $error_zone; ?></span>
              <?php } ?>
            </div>
          </div>
        </div>

        <h5><?php echo $text_your_password; ?></h5>
        <div class="box">
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_password; ?></label>
            <div class="form-control-wrap">
              <input type="password" class="form-control" name="password" value="<?php echo $password; ?>"/>
              <?php if ($error_password) { ?>
              <span class="error"><?php echo $error_password; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label"><span class="required">*</span> <?php echo $entry_confirm; ?></label>
            <div class="form-control-wrap">
              <input type="password" class="form-control" name="confirm" value="<?php echo $confirm; ?>"/>
              <?php if ($error_confirm) { ?>
              <span class="error"><?php echo $error_confirm; ?></span>
              <?php } ?>
            </div>
          </div>
        </div>

        <h5><?php echo $text_newsletter; ?></h5>
        <div class="box">
          <div class="form-group">
            <label><?php echo $entry_newsletter; ?></label>
            <div class="form-control-wrap">
              <?php if ($newsletter) { ?>
              <input type="radio" name="newsletter" value="1" checked="checked"/>
              <?php echo $text_yes; ?>
              <input type="radio" name="newsletter" value="0"/>
              <?php echo $text_no; ?>
              <?php } else { ?>
              <input type="radio" name="newsletter" value="1"/>
              <?php echo $text_yes; ?>
              <input type="radio" name="newsletter" value="0" checked="checked"/>
              <?php echo $text_no; ?>
              <?php } ?>
            </div>
          </div>
        </div>
        <?php if ($text_agree) { ?>
        <div class="buttons">
          <div class="fr"><?php echo $text_agree; ?>
            <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked"/>
            <?php } else { ?>
            <input type="checkbox" name="agree" value="1"/>
            <?php } ?>
            <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
          </div>
        </div>
        <?php } else { ?>
        <div class="buttons">
          <div class="fr">
            <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
          </div>
        </div>
        <?php } ?>
      </form>
    </div>
  </div>
  <script type="text/javascript"><!--
    $('input[name=customer_group_id]').filter(':checked').on('change', function () {
      var customer_group = [];

      <?php foreach($customer_groups as $customer_group) { ?>
        customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
        customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
        customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
        customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
        customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
      <?php } ?>

      if (customer_group[this.value]) {
        if (customer_group[this.value]['company_id_display'] == '1') {
          $('#company-id-display').show();
        } else {
          $('#company-id-display').hide();
        }

        if (customer_group[this.value]['company_id_required'] == '1') {
          $('#company-id-required').show();
        } else {
          $('#company-id-required').hide();
        }

        if (customer_group[this.value]['tax_id_display'] == '1') {
          $('#tax-id-display').show();
        } else {
          $('#tax-id-display').hide();
        }

        if (customer_group[this.value]['tax_id_required'] == '1') {
          $('#tax-id-required').show();
        } else {
          $('#tax-id-required').hide();
        }
      }
    });

    $('input[name=\'customer_group_id\']:checked').trigger('change');
    //--></script>
  </div>
</main>
<?php echo $footer; ?>