<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    
        <?php echo $column_left; ?>
             
        <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="contact-info">
          <p><?php echo $text_email; ?></p><br/>
          <div class="box">
            <div class="form-group">
              <label class="form-label"><?php echo $entry_email; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="email" value="" class="form-control" />
              </div>
            </div>
          </div>
        </div>
        <div class="buttons cf">
          <div class="fl">
            <a href="<?php echo $back; ?>" class="btn btn-inline"><?php echo $button_back; ?></a>
          </div>
          <div class="fr">
            <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>
<?php echo $footer; ?>