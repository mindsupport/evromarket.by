<?php echo $header; ?>
<main class="wrap">
  <div class="container">
    <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
        <?php echo $column_left; ?>
        
        <?php echo $column_right; ?>

    <div class="content"><?php echo $content_top; ?>
<?php echo $this->renderPartial('breadcrumbs'); ?>

      <h3><?php echo $heading_title; ?></h3>
      <div class="account">
        <h5><?php echo $text_address_book; ?></h5>
        <?php foreach ($addresses as $result) { ?>
        <div class="box">
          <table style="width: 100%;">
            <tr>
              <td><?php echo $result['address']; ?></td>
              <td style="text-align: right;"><a href="<?php echo $result['update']; ?>" class="button"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="button"><?php echo $button_delete; ?></a></td>
            </tr>
          </table>
        </div>
        <?php } ?>
        <div class="buttons">
          <div class="fl">
            <a href="<?php echo $back; ?>" class="btn btn-inline"><?php echo $button_back; ?></a>
          </div>
          <div class="fr">
            <a href="<?php echo $insert; ?>" class="btn btn-inline"><?php echo $button_new_address; ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<?php echo $footer; ?>