<?php

echo $header; ?>

<main class="wrap">
    <div class="container">

        <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>

       <?php echo $column_left; ?>
           <?php echo $column_right; ?>
        <div class="content">

            <?php echo $content_top; ?>

<?php echo $this->renderPartial('breadcrumbs'); ?>

            <h3><?php echo $heading_title; ?></h3>
              <?php echo $text_description; ?>
            <br />

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

                <div class="login-page cf">

                    <div class="fl">
                        <h5><?php echo $text_order; ?></h5>

                        <div class="box">

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_firstname; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="form-control" />

                                    <?php if ($error_firstname) { ?>
                                    <span class="error"><?php echo $error_firstname; ?></span>
                                <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_lastname; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="form-control" />

                            <?php if ($error_lastname) { ?>
                                    <span class="error"><?php echo $error_lastname; ?></span>
                            <?php } ?>

                                </div>
                            </div> 

                            <div class="form-group">
                                <label><span class="required">*</span><?php echo $entry_email; ?></label>
                                <div class="form-control-wrap">
                                    <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" />

                            <?php if ($error_email) { ?>
                                    <span class="error"><?php echo $error_email; ?></span>
                            <?php } ?>

                                </div>
                            </div> 

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_telephone; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="form-control" />

                            <?php if ($error_telephone) { ?>
                                    <span class="error"><?php echo $error_telephone; ?></span>
                            <?php } ?>

                                </div>
                            </div> 

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_order_id; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="order_id" value="<?php echo $order_id; ?>" class="form-control" />

                            <?php if ($error_order_id) { ?>
                                    <span class="error"><?php echo $error_order_id; ?></span>
                            <?php } ?>

                                </div>
                            </div> 

                            <div class="form-group">
                                <label><?php echo $entry_date_ordered; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" class="date form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo $entry_captcha; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="captcha" class="form-control" value="<?php echo $captcha; ?>" />
                                    <img class="captcha" src="index.php?route=account/return/captcha" alt="" />

                                            <?php if ($error_captcha) { ?>
                                    <span class="error"><?php echo $error_captcha; ?></span>
            <?php } ?>
                                </div>                           
                            </div>

                            <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>

                        </div>
                    </div>

                    <div class="fr">
                        <h5><?php echo $text_product; ?></h5>

                        <div class="box">
                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_product; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="product" value="<?php echo $product; ?>" class="form-control" />

                            <?php if ($error_product) { ?>
                                    <span class="error"><?php echo $error_product; ?></span>
                            <?php } ?>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_model; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="model" value="<?php echo $model; ?>" class="form-control" />

                            <?php if ($error_model) { ?>
                                    <span class="error"><?php echo $error_model; ?></span>
                            <?php } ?>
                                </div>                           
                            </div>

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_quantity; ?></label>
                                <div class="form-control-wrap">
                                    <input type="text" name="quantity" value="<?php echo $quantity; ?>" class="form-control" />
                                </div>                           
                            </div> 

                            <div class="form-group">
                                <label><span class="required">*</span> <?php echo $entry_reason; ?></label>
                                <div class="form-control-wrap">



              <?php foreach ($return_reasons as $return_reason) { ?>
              <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>

                                    <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" checked="checked" /> &mdash;
                                <?php echo $return_reason['name']; ?>

              <?php } else { ?>

                                    <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" /> &mdash; <?php echo $return_reason['name']; ?>

              <?php  } ?>
                                    <br>
              <?php  } ?>


<?php if ($error_reason) { ?>
                                    <span class="error"><?php echo $error_reason; ?></span>
            <?php } ?>

                                </div>                           
                            </div>

                            <div class="form-group">
                                <label><?php echo $entry_opened; ?></label>
                                <div class="form-control-wrap">

                                <?php if ($opened) { ?>
                                    <input type="radio" name="opened" value="1" id="opened" checked="checked" />
            <?php } else { ?>
                                    <input type="radio" name="opened" value="1" id="opened" />
            <?php } ?>
                                    &mdash; <?php echo $text_yes; ?> <br />
            <?php if (!$opened) { ?>
                                    <input type="radio" name="opened" value="0" id="unopened" checked="checked" />
            <?php } else { ?>
                                    <input type="radio" name="opened" value="0" id="unopened" />
            <?php } ?>
                                    &mdash; <?php echo $text_no; ?>

                                </div>                           
                            </div> 

                            <div class="form-group">
                                <label><?php echo $entry_fault_detail; ?></label>
                                <div class="form-control-wrap">
                                    <textarea name="comment" cols="150" class="form-control" rows="6"><?php echo $comment; ?></textarea>
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</main>


<?php echo $footer; ?>