<?php if(!empty($products)): ?>
<div class="container">
    <h3 class="center upper"><?php echo $heading_title; ?></h3>
    <div class="products-list">
        <div class="center">
            <a class="carousel-prev" href="#"></a>
            <a class="carousel-next" href="#"></a>
        </div>
        <div class="carousel">
            <ul>
      <?php foreach ($products as $product) { ?>
                <li>
                    <?php echo $this->renderPartial('product', array('product' => $product)); ?>
                </li>
      <?php } ?>
            </ul>
        </div>
        <div class="center full-view">
            <a href="#">Посмотреть все</a>
        </div>
    </div>
</div>

<hr>

<?php endif; ?>