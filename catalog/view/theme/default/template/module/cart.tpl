<div class="cart-mini" id="cart">
    <span class="cart-mini-icon">
        <i class="icon icon-cart-dd"></i>
    </span>
    <span id="cart-total">
        <?php echo $text_items; ?>
    </span>

    <div class="cart-content">
    <?php if ($products || $vouchers) { ?>
        <div class="mini-cart-info">
            <table>
        <?php foreach ($products as $product) { ?>
                <tr>
                    <td class="col-image"><?php if ($product['thumb']) { ?>
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?></td>
                    <td class="col-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <div>
              <?php foreach ($product['option'] as $option) { ?>
                            - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br />
              <?php } ?>
              <?php if ($product['recurring']): ?>
                            - <small><?php echo $text_payment_profile ?> <?php echo $product['profile']; ?></small><br />
              <?php endif; ?>
                        </div></td>
                    <td class="col-quantity">x&nbsp;<?php echo $product['quantity']; ?></td>
                    <td class="col-total"><?php echo $product['total']; ?></td>
                    <td>
                      <button type="button" class="remove remove-mini" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $product['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $product['key']; ?>' + ' #cart > *');">x</button>
                    </td>
                </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                    <td class="col-image"></td>
                    <td class="col-name"><?php echo $voucher['description']; ?></td>
                    <td class="col-quantity">x&nbsp;1</td>
                    <td class="col-total"><?php echo $voucher['amount']; ?></td>
                    <td>
                      <button type="button" class="remove remove-mini" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $voucher['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $voucher['key']; ?>' + ' #cart > *');">x</button>
                    </td>
                </tr>
        <?php } ?>
            </table>
        </div>
        <div class="mini-cart-total">
            <table>
        <?php foreach ($totals as $total) { ?>
                <tr>
                    <td class="right"><?php echo $total['title']; ?>:</td>
                    <td class="right"><?php echo $total['text']; ?></td>
                </tr>
        <?php } ?>
            </table>
        </div>
        <div class="checkout"><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a> | <a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></div>
    <?php } else { ?>
        <div class="empty"><?php echo $text_empty; ?></div>
    <?php } ?>
    </div>

</div>

