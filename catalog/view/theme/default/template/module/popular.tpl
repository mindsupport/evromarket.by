<div class="footer-news">
    <h6><?php echo $heading_title; ?></h6>
    <ul>
                     <?php foreach ($products as $product) { ?>
        <li>
                        <?php if ($product['thumb']) { ?>
            <figure><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></figure>
                          <?php } ?>

            <div class="news-text">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                
                <?php if(!empty($product['description'])): ?>
                    <p><?php echo $product['description']; ?></p>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
        </li>
                     <?php } ?>
    </ul>
</div>
