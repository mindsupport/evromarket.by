<?php

if(!empty($filter_groups)): ?>

<div class="sidebar" style="clear: both;">

    <?php if(!empty($filter_category)): ?>
    <div class="filter-list">
        <div class="filter-title">
            <p> <a href="#" class="filter-link-reset">Сбросить</a></p>
        </div>
    </div>
<?php endif; ?>

     <?php foreach ($filter_groups as $filter_group) { ?>
    <div class="filter-list">
        <div class="filter-title">
            <p id="filter-group<?php echo $filter_group['filter_group_id']; ?>">
                <?php echo $filter_group['name']; ?>
            </p>
        </div>
        <ul>
           <?php foreach ($filter_group['filter'] as $filter) { ?>
          <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
            <li>
                <a href="#" class="filter-link selected" id="filter<?php echo $filter['filter_id']; ?>" data-filter-id="<?php echo $filter['filter_id']; ?>">
                    <i class="icon icon-checked"></i>
                    <?php echo $filter['name']; ?>
                </a>
            </li>
          <?php } else { ?>
            <li>
                <a href="#" class="filter-link" id="filter<?php echo $filter['filter_id']; ?>" data-filter-id="<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></a>
            </li>
          <?php } ?>
          <?php } ?>
        </ul>
    </div>
     <?php } ?>
</div>

<script type="text/javascript">

    $(function() {

        filters = ($.totalStorage('filters') === null) ? [] : $.totalStorage('filters');

        function toUnique(a, b, c) {
            b = a.length;
            while (c = --b) {
                while (c--) {
                    a[b] !== a[c] || a.splice(c, 1);
                }
            }
            return a;
        }

        $('.filter-link').on('click', function() {
            filterId = parseInt($(this).attr('data-filter-id'));
            filters.push(filterId);
            filters = toUnique(filters);
            $.totalStorage('filters', filters);
            window.location = '<?php echo $action; ?>&filter=' + filters.join(',');
            return false;
        });

        $('.filter-link-reset').on('click', function() {
            $.totalStorage('filters', null);
            window.location = '<?php echo $action; ?>';
            return false;
        });
    });
</script>

<?php endif; ?>
