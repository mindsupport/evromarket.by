<?php

if(!empty($products)): ?> 
<div class="content">
    <div class="home-carousel">
        <div class="carousel">
            <ul>

   <?php foreach ($products as $product) { ?>
                <li>
        <?php if ($product['thumb']) { ?>
                    <figure>
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                    </figure>
        <?php } ?>
                    <div class="product-desc">

                        <h2><?php echo $product['name']; ?></h2>

                            <?php if ($product['price']) { ?>
                        <div class="price">
                            <div class="fl">
          <?php if (!$product['special']) { ?>
                                <p><?php echo $product['price']; ?></p>
          <?php } else { ?>
                                <p class="old"><?php echo $product['price']; ?></p>
                                <p><?php echo $product['special']; ?></p> 

          <?php } ?>
                            </div>
                        </div>
        <?php } ?>
                        <p class="desc-text">
                         <?php echo $product['description']; ?>
                        </p>
                    </div>
                </li>
      <?php } ?>
            </ul>
        </div>
        
        <a class="carousel-prev" href="#"></a>
        <a class="carousel-next" href="#"></a>
        
        <div class="carousel-pagination"></div>
        
    </div>
    <div class="reasons" id="reasons">
        <h2>8 ПРИЧИН ПОКУПАТЬ В НАШЕМ МАГАЗИНЕ</h2>
        <div class="carousel">
            <ul>
                <li>
                    <p>
                        Официальная гарантия
                        на все товары
                    </p>
                </li>
                <li>
                    <p>
                        Доставка по всей беларуси
                    </p>
                </li>
                <li>
                    <p>
                        Широкий ассортемент
                        более 150 000 товаров
                    </p>
                </li>
                <li>
                    <p>
                        Официальная гарантия
                        на все товары
                    </p>
                </li>
                <li>
                    <p>
                        Доставка по всей беларуси
                    </p>
                </li>
                <li>
                    <p>
                        Широкий ассортемент
                        более 150 000 товаров
                    </p>
                </li>
                <li>
                    <p>
                        Официальная гарантия
                        на все товары
                    </p>
                </li>
                <li>
                    <p>
                        Доставка по всей беларуси
                    </p>
                </li>
            </ul>
        </div>
        <a class="carousel-next" href="#"></a>
    </div>

</div>
<?php endif; ?>