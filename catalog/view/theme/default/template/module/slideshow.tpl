<?php if(!empty($banners)): ?>
<hr/>
<div class="container">
    <h4 class="center upper">Бренды</h4>
    <div class="brands">
        <div class="carousel">
            <ul>
               <?php foreach ($banners as $banner) { ?>
                <li>
    <?php if ($banner['link']) { ?>
                    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
                </li>
    <?php } ?> 
            </ul>
        </div>
        <a class="carousel-prev" href="#"></a>
        <a class="carousel-next" href="#"></a>
    </div>
</div>
<?php endif; ?>