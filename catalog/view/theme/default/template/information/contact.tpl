<?php echo $header; ?>

<main class="wrap">
  <div class="container">
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
     
        <?php echo $this->renderPartial('breadcrumbs'); ?>
        
      <h3><?php echo $heading_title; ?></h3>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <h5><?php echo $text_location; ?></h5>
        <div class="contact-info">
          <div class="box">
            <div class="fl col-half">
              <strong><?php echo $text_address; ?></strong><br/>
              <p><?php echo $store; ?></p>
              <p><?php echo $address; ?></p>
            </div>
            <div class="fr col-half">
              <?php if ($telephone) { ?>
                <strong><?php echo $text_telephone; ?></strong><br/>
                <p><?php echo $telephone; ?></p>
              <?php } ?>
              <?php if ($fax) { ?>
                <strong><?php echo $text_fax; ?></strong><br/>
                <p><?php echo $fax; ?></p>
              <?php } ?>
            </div>
          </div>
        </div>

        <h5><?php echo $text_contact; ?></h5>
        <div class="contact-info">
          <div class="box">
            <div class="form-group">
              <label class="form-label"><?php echo $entry_name; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="name" value="<?php echo $name; ?>" class="form-control" />
                <?php if ($error_name) { ?>
                <span class="error"><?php echo $error_name; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_email; ?></label>
              <div class="form-control-wrap">
                <input type="email" name="name" value="<?php echo $email; ?>" class="form-control" />
                <?php if ($error_email) { ?>
                <span class="error"><?php echo $error_email; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_enquiry; ?></label>
              <div class="form-control-wrap">
                <textarea class="form-control"><?php echo $enquiry; ?></textarea>
                <?php if ($error_enquiry) { ?>
                <span class="error"><?php echo $error_enquiry; ?></span>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label"><?php echo $entry_captcha; ?></label>
              <div class="form-control-wrap">
                <input type="text" name="name" value="<?php echo $captcha; ?>" class="form-control" />
                <br/><br/>
                <img src="index.php?route=information/contact/captcha" alt=""/>
                <?php if ($error_captcha) { ?>
                <span class="error"><?php echo $error_captcha; ?></span>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <div class="buttons">
          <div class="fr">
            <button type="submit" class="btn btn-inline"><?php echo $button_continue; ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>
<?php echo $footer; ?>