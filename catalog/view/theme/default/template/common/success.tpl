<?php

echo $header; ?>

<main class="wrap">
    <div class="container">
 <?php echo $column_left; ?>
        <?php echo $column_right; ?>
        <div class="content"><?php echo $content_top; ?>
            <?php echo $this->renderPartial('breadcrumbs'); ?>

            <h3><?php echo $heading_title; ?></h3>

      <?php echo $text_message; ?><br/>

            <div class="buttons">
                <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
            </div>
        </div>
    </div>
</main>

<?php echo $footer; ?>