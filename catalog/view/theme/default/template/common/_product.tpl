<div class="tabs">
    <div class="tabs-controls">
        <a href="#desc" class="tab-control">Описание</a>
        <a href="#options" class="tab-control">Варианты (<?php echo count($product['related_products_by_attributes']); ?>)</a>
    </div>
    <div class="tabs-content">
        <div class="tab-content" data-tab="desc">
            <div class="product-over">
<?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
        <?php } ?>  
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>

                <?php if(!empty($product['description'])): ?>
                <div class="desc-over"><?php echo $product['description']; ?></div>
                <?php endif; ?>
            </div>


        <?php if ($product['price']) {?>

          <?php if (!$product['special']) { ?>
            <p class="price"><?php echo $product['price']; ?></p>
          <?php } else { ?>
            <p class="old-price"><?php echo $product['price']; ?></p>

            <p class="price"><?php echo $product['special']; ?></p>
          <?php } ?>

        <?php } ?>
        <?php /*if ($product['rating']) { ?>
                                <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php }*/ ?>

            <a href="#" class="add-to-cart btn" onclick="addToCart('<?php echo $product['product_id']; ?>');
                    return false;">
                <i class="icon icon-cart"></i>
                Добавить в корзину
            </a>

            <div class="add-to-compare">
                <a href="#" onclick="addToCompare('<?php echo $product['product_id']; ?>');
                        return false;">
                    <i class="icon icon-compare"></i>
                    Добавить в сравнение
                </a>
            </div>
        </div>
        <?php if(!empty($product['related_products_by_attributes'])): ?>
        <div class="tab-content" data-tab="options">
            <ul class="options">
                <?php foreach($product['related_products_by_attributes'] as $product_related): ?>
                <li>
                    <div class="cf">
                        <figure>
                            <?php if (!empty($product_related['thumb'])) { ?>
                            <a href="<?php echo $this->url->link('product/product', 'product_id=' . $product_related['product_id']); ?>"><img src="<?php echo $product_related['thumb']; ?>" alt="<?php echo $product_related['name']; ?>" /></a>
        <?php } ?>
                        </figure>
                        <div class="option-desc">
                            <?php if(!empty($product_related['description'])): ?>
                            <p><a href="<?php echo $this->url->link('product/product', 'product_id=' . $product_related['product_id']); ?>">
                                <?php echo utf8_substr(strip_tags(html_entity_decode($product_related['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..'; ?></a>
                            </p>
                            <?php endif; ?>
                                    <?php if ($product_related['price']) {?>


                            <?php 
                            
                            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) { 
                    $price = $this->currency->format($this->tax->calculate($product_related['price'], $product_related['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }
                            
                            ?>

                            <p class="price"><?php echo $price; ?></p>


        <?php } ?>
                        </div>
                    </div>
                    <?php if(!empty($product_related['options'])): ?>
                    <p class="option-text">
                        options...
                    </p>
                    <?php endif; ?>

                    <div class="add-to-compare">
                        <a href="#" onclick="addToCompare('<?php echo $product_related['product_id']; ?>');
                                return false;">
                            <i class="icon icon-compare"></i>
                            Добавить в сравнение
                        </a>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>


            <!--            <div class="options">
                <?php foreach($product['related_products_by_attributes'] as $product_related): ?>
                            <div class="product-over">

                                <a href="<?php echo $this->url->link('product/product', 'product_id=' . $product_related['product_id']); ?>"><?php echo $product_related['name']; ?></a>
            
                <?php if(!empty($product_related['description'])): ?>
                                <div class="desc-over"><?php echo html_entity_decode($product_related['description'], ENT_QUOTES, 'UTF-8'); ?></div>
                <?php endif; ?>
                            </div>
            
            
        <?php if ($product_related['price']) {?>
            
          <?php if (!$product_related['special']) { ?>
                            <p class="price"><?php echo $product_related['price']; ?></p>
          <?php } else { ?>
                            <p class="old-price"><?php echo $product_related['price']; ?></p>
            
                            <p class="price"><?php echo $product_related['special']; ?></p>
          <?php } ?>
            
        <?php } ?>
                            <a href="#" class="add-to-cart btn" onclick="addToCart('<?php echo $product_related['product_id']; ?>');
                                    return false;">
                                <i class="icon icon-cart"></i>
                                Добавить в корзину
                            </a>
            
                            <div class="add-to-compare">
                                <a href="#" onclick="addToCompare('<?php echo $product_related['product_id']; ?>');
                                        return false;">
                                    <i class="icon icon-compare"></i>
                                    Добавить в сравнение
                                </a>
                            </div>
                <?php endforeach; ?>
                        </div>-->
        </div>
        <?php endif; ?>
    </div>
</div>