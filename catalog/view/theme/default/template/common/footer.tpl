<!-- footer start -->
<footer>
    <div class="container">

<?php echo $content_bottom; ?>

        <div class="fl">
            <?php echo $popular; ?>
            <div class="footer-links">
                <div class="cf">
                    <div class="fl">
                        <p>Информация</p>


                        <ul>
                            <?php foreach($informations as $info): ?>
                            <li>
                                <a href="<?php echo $info['href']; ?>"><?php echo $info['title']; ?></a>
                            </li>
                        <?php endforeach; ?>
                            <li>
                                <a href="<?php echo $manufacturer; ?>">Производители (бренды)</a>
                            </li>
                            <li>
                                <a href="<?php echo $voucher; ?>">Подарочные сертификаты</a>
                            </li>
<!--                            <li>
                                <a href="<?php echo $affiliate; ?>">Партнёрская программа</a>
                            </li>-->
                            <li>
                                <a href="<?php echo $special; ?>">Акции</a>
                            </li>
                        </ul>
                    </div>
                    <div class="fl">
                        <p>Личный Кабинет</p>
                        <ul>
                            <li>
                                <a href="<?php echo $account; ?>">Личный Кабинет</a>
                            </li>
                            <li>
                                <a href="<?php echo $order; ?>">История заказов</a>
                            </li>
                            <li>
                                <a href="<?php echo $wishlist; ?>">Закладки</a>
                            </li>
                            <li>
                                <a href="<?php echo $newsletter; ?>">Рассылка</a>
                            </li>
                        </ul>
                    </div>
                    <div class="fl">
                        <p>Служба поддержки</p>
                        <ul>
                            <li>
                                <a href="<?php echo $contact; ?>">Связаться с нами</a>
                            </li>
                            <li>
                                <a href="<?php echo $return; ?>">Возврат товара</a>
                            </li>
                            <li>
                                <a href="<?php echo $sitemap; ?>">Карта сайта</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="shop-info">
                    <p class="title">Информация о магазине</p>
                    <?php echo $powered; ?>
                </div>
            </div>
        </div>
    </div>
</footer><!-- footer end -->

</body>
</html>