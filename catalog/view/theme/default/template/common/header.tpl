<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?php echo $base; ?>" />
        <title>evromarket</title>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <?php foreach ($links as $link) { ?>
        <link href="/<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>

<?php foreach ($styles as $style) { ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="/<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>

        <link rel="stylesheet" type="text/css" href="/css/styles.css">    
        <link rel="stylesheet" type="text/css" href="/css/more.css">   
       <!--<script src="/js/all.min.js"></script>-->
        <script src="/js/all.js"></script>
        <script type="text/javascript" src="/catalog/view/javascript/jquery/ui/jquery-ui.min.js"></script>
        <script src="/catalog/view/javascript/common.js"></script>

<?php foreach ($scripts as $script) { ?>
        <script type="text/javascript" src="/<?php echo $script; ?>"></script>
<?php } ?>

        <!--[if lt IE 9]>
        <script src="/js/lib/html5shiv.min.js"></script>
        <![endif]-->
    </head>

    <body>
        
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'ZqBvAM1jNX';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->



        <!-- header start -->
        <header>
            <div class="top-bar">
                <div class="container">

                    <?php echo $cart; ?>

                    <nav class="main-nav">
                        <a class="home-link <?php if(!isset($this->request->get['route'])): ?>active<?php endif;?>" href="/"><i class="icon icon-home"></i></a>
                        <ul>
                            <li><a href="<?php echo $catalog_link; ?>">Каталог</a></li>
                            <?php if(!empty($infos)): ?>
                                <?php foreach($infos as $inf): ?>
                            <li>
                                <a href="<?php echo $inf['href']; ?>" <?php if(isset($this->request->get['route']) && isset($this->request->get['information_id']) && $this->request->get['route'] == 'information/information' && $this->request->get['information_id'] == $inf['id']): ?>class="active"<?php endif;?>>
                                        <?php echo $inf['title']; ?>
                                </a>
                            </li>
                                <?php endforeach; ?>
                            <li><a href="<?php echo $contact_url; ?>" <?php if(isset($this->request->get['route']) && $this->request->get['route'] == 'information/contact'): ?>class="active"<?php endif;?>>Контакты</a></li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="header-info">
                <div class="container">
                    <h1 class="logo"><a href="/">evromarket</a></h1>

                    <div class="cf">
                        <div class="fr">
                            <p><a href="<?php echo $compare; ?>" id="compare-total-header"><?php echo $text_compare; ?></a></p>
                            <p><a href="<?php echo $shopping_cart; ?>">Корзина</a></p>
                        </div>
                        <div class="fr">
                            <?php if(!$logged): ?>
                            <p>Добро пожаловать!</p>
                            <p>
                                <a href="<?php echo $login_link; ?>">Войти</a> или <a href="<?php echo $register_link; ?>">зарегистрироваться</a>
                            </p>
                            <?php else: ?>
                            <p>
                                <?php if($this->customer->getFirstName()): ?>
                                    <?php echo $this->customer->getFirstName(); ?>
                                <?php endif; ?>

                               <?php if($this->customer->getLastName()): ?>
                                    <?php echo $this->customer->getLastName(); ?>
                                <?php endif; ?>
                            </p>
                            <p>
                                <a href="<?php echo $logout; ?>">Выйти</a> / <a href="<?php echo $account; ?>">настройки</a>
                            </p>

                            <?php endif; ?>
                        </div>
                        <div class="fr">
                            <div class="fr">

                                <?php if(!empty($skype)): ?>
                                <p>skype: <a href="skype:<?php echo $skype; ?>?chat"><?php echo $skype; ?></a></p>
                                <?php endif; ?>

                                 <?php if(!empty($viber)): ?>
                                <p>viber: <a href="tel:<?php echo $viber; ?>"><?php echo $viber; ?></a></p>
                                 <?php endif; ?>

                            </div>
                            <div class="fr">

                                <?php if(!empty($velcom)): ?>
                                <p><a href="tel:<?php echo $velcom; ?>" class="tel"><?php echo $velcom; ?></a> (velcom)</p>
                                 <?php endif; ?>

                                <?php if(!empty($mts)): ?>
                                <p><a href="tel:<?php echo $mts; ?>" class="tel"><?php echo $mts; ?></a> (мтс)</p>
                                 <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search">
                <div class="container">
                    <button type="submit" class="submit btn button-search">Найти</button>
                    <div class="field-wrap">
                        <i class="icon icon-search"></i>
                        <input class="field search-from-head" placeholder="Поиск" type="text"  name="search" value="<?php echo $search; ?>"/>
                    </div>

                    <div id="notification"></div>
                </div>
            </div>
        </header><!-- header end -->