<?php echo $header; ?>
<main class="wrap">
  <div class="container">
 <?php echo $column_left; ?>
        
        <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
      
        <?php echo $this->renderPartial('breadcrumbs'); ?>
        
      <h3><?php echo $heading_title; ?></h3>
      <?php if ($categories) { ?>
      <p><strong><?php echo $text_index; ?></strong>
        <?php foreach ($categories as $category) { ?>
        &nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><strong><?php echo $category['name']; ?></strong></a>
        <?php } ?>
      </p>
      <br/>
      <?php foreach ($categories as $category) { ?>
      <div class="manufacturer-list">
        <div class="manufacturer-heading"><?php echo $category['name']; ?><a id="<?php echo $category['name']; ?>"></a></div>
        <div class="manufacturer-content">
          <?php if ($category['manufacturer']) { ?>
          <?php for ($i = 0; $i < count($category['manufacturer']);) { ?>
          <ul>
            <?php $j = $i + ceil(count($category['manufacturer']) / 4); ?>
            <?php for (; $i < $j; $i++) { ?>
            <?php if (isset($category['manufacturer'][$i])) { ?>
            <li><a href="<?php echo $category['manufacturer'][$i]['href']; ?>"><?php echo $category['manufacturer'][$i]['name']; ?></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
      <?php } else { ?>
      <div class="box"><?php echo $text_empty; ?></div>
      <div class="buttons">
        <div class="fr"><a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
    </div>
  </div>
</main>
<?php echo $footer; ?>