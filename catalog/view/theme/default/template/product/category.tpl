<?php

echo $header; ?>
<main class="wrap">
    <div class="container">


            <?php echo $column_left; ?>

<?php if(!empty($manufacturer_filters)) { ?>
        <div class="sidebar" style="clear: both;">  
            <div class="filters">
                <div class="filter-list">
                    <div class="filter-title">
                        <p>Производитель</p>
                    </div>

                    <ul>
                    <?php foreach($manufacturer_filters as $m) { ?>
                        <li>
                            <a href="<?php echo $m['href']; ?>" <?php if(!empty($manufacturer_filter) && $m['value'] == $manufacturer_filter): ?>class="selected"<?php endif; ?>>

                             <?php if(!empty($manufacturer_filter) && $m['value'] == $manufacturer_filter): ?>
                                <i class="icon icon-checked"></i>
                            <?php endif; ?>

                            <?php echo $m['text']; ?>
                            </a>
                        </li>
                    <?php } ?>
                    </ul>

                </div>
            </div>        
        </div> 
<?php } ?>

    <?php // echo $column_right; ?>

        <div class="content">
      <?php // echo $content_top; ?>

<?php echo $this->renderPartial('breadcrumbs'); ?>

            <div class="catalog">
                <h3 class="upper"><?php echo $heading_title; ?></h3>

        <?php /*if ($thumb) { ?>
        <div class="image">
          <!--<img src="<?php echo $thumb; ?>">-->
        </div>
        <?php }*/ ?>

        <?php if ($description) { ?>
                <div class="catalog-desc">
          <?php echo $description; ?>
                </div>
        <?php } ?>

        <?php if ($products) { ?>
                <div class="sort">
                  <!--<b><?php echo $text_sort; ?></b>-->
                    <select onchange="location = this.value;" class="form-control">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
                    </select>
                </div>
        <?php } ?>

        <?php if ($products) { ?>
                <div class="products-list">
                    <ul>
            <?php foreach ($products as $product) { ?>
                        <li>
                    <?php echo $this->renderPartial('product', array('product' => $product)); ?>
                        </li>
            <?php } ?>
                    </ul>
                </div>

                <div class="pagination">
          <?php echo $pagination; ?>
                </div>

        <?php } ?>

            </div>

      <?php if ($categories) { ?>
      <!--<h2><?php echo $text_refine; ?></h2>-->
            <!--<div class="category-list">
      <?php if (count($categories) <= 5) { ?>
                <ul>
      <?php foreach ($categories as $category) { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
      <?php } ?>
                </ul>
      <?php } else { ?>
      <?php for ($i = 0; $i < count($categories);) { ?>
                <ul>
      <?php $j = $i + ceil(count($categories) / 4); ?>
      <?php for (; $i < $j; $i++) { ?>
      <?php if (isset($categories[$i])) { ?>
                    <li><a href="<?php echo $categories[$i]['href']; ?>"><?php echo $categories[$i]['name']; ?></a></li>
      <?php } ?>
      <?php } ?>
                </ul>
      <?php } ?>
      <?php } ?>
            </div>-->
      <?php } ?>


      <?php if ($products) { ?>
            <!--            <div class="product-filter">
                            <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
                            <div class="limit"><b><?php echo $text_limit; ?></b>
                                <select onchange="location = this.value;">
      <?php foreach ($limits as $limits) { ?>
      <?php if ($limits['value'] == $limit) { ?>
                                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
      <?php } else { ?>
                                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
      <?php } ?>
      <?php } ?>
                                </select>
                            </div>
                        </div>-->
            <!--<div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>-->
            <!--
            <div class="product-list">
      <?php foreach ($products as $product) { ?>
                            <div>
      
                                <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      
      <?php if (!$product['special']) { ?>
      <?php echo $product['price']; ?>
      <?php } else { ?>
                                    <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
      <?php } ?>
      <?php if ($product['tax']) { ?>
                                    <br />
                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
      <?php } ?>
      
      <?php } ?>
      
      <?php if ($product['rating']) { ?>
                                <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
      <?php } ?>
                                <div class="cart">
                                    <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
                                </div>
                                <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
                                <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>
                            </div>
      <?php } ?>
                        </div>
            -->
            <!--<div class="pagination"><?php echo $pagination; ?></div>-->
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
            <div class="content"><?php echo $text_empty; ?></div>
      <?php } ?>

        </div>
    </div>
  <?php echo $content_top; ?>
</main>

<?php echo $footer; ?>