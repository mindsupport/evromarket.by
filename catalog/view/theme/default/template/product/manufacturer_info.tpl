<?php echo $header; ?>

<main class="wrap">
  <div class="container">
 <?php echo $column_left; ?>
      
      <?php echo $column_right; ?>
    <div class="content"><?php echo $content_top; ?>
        
      <?php echo $this->renderPartial('breadcrumbs'); ?>

      <div class="catalog">
        <h3 class="upper"><?php echo $heading_title; ?></h3>
        <?php if ($products) { ?>
        <!--<div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
        <div class="limit"><?php echo $text_limit; ?>
          <select onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>-->
        <div class="sort">
          <select onchange="location = this.value;" class="form-control">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>"
                    selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <!--<div class="product-compare"><a href="<?php echo $compare; ?>"
                                        id="compare-total"><?php echo $text_compare; ?></a></div>-->
        <div class="products-list">
          <ul>
            <?php foreach ($products as $product) { ?>
             <li>
                    <?php echo $this->renderPartial('product', array('product' => $product)); ?>
                </li>
            <?php } ?>
          </ul>
        </div>
        <div class="pagination"><?php echo $pagination; ?></div>
        <?php } else { ?>
        <div class="empty">
          <div class="buttons">
            <div class="fr">
              <a href="<?php echo $continue; ?>" class="btn btn-inline"><?php echo $button_continue; ?></a>
            </div>
          </div>
          <p><?php echo $text_empty; ?></p>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</main>
<?php echo $footer; ?>