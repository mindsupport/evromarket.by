<?php

echo $header; ?>
<main class="wrap">
    <div class="container">

 <?php echo $column_left; ?>

      <?php echo $column_right; ?>
        <div class="content"><?php echo $content_top; ?>

      <?php echo $this->renderPartial('breadcrumbs'); ?>

            <div class="catalog">
                <h3 class="upper"><?php echo $heading_title; ?></h3>

        <?php if ($products) { ?>
                <div class="sort">
                    <select onchange="location = this.value;" class="form-control">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
                    </select>
                </div>
                <div class="products-list">
                    <ul>
              <?php foreach ($products as $product) { ?>
                        <li>
                    <?php echo $this->renderPartial('product', array('product' => $product)); ?>
                        </li>
              <?php } ?>
                    </ul>
                </div>
              <!--<div class="pagination"><?php echo $pagination; ?></div>-->
        <?php } else { ?>
                <div class="empty"><?php echo $text_empty; ?></div>
        <?php }?>
            </div>
        </div>
    </div>
</main>
<?php echo $footer; ?>