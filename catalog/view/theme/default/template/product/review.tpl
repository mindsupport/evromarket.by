<?php

if ($reviews) { ?>

<ul class="reviews">



<?php foreach ($reviews as $review) { ?>

<li>
    <p class="name">
        <?php echo $review['author']; ?> 
        <img src="catalog/view/theme/default/image/stars-<?php echo $review['rating'] . '.png'; ?>" alt="<?php echo $review['reviews']; ?>" />
<!-- &mdash; <?php echo $text_on; ?> <?php echo $review['date_added']; ?> -->
    </p>
    <p>
        <?php echo $review['text']; ?> 
    </p>
</li>

<!--<div class="review-list">
  <div class="author"><b></b> <?php echo $text_on; ?> <?php echo $review['date_added']; ?></div>
  <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $review['rating'] . '.png'; ?>" alt="<?php echo $review['reviews']; ?>" /></div>
  <div class="text"></div>
</div>-->

<?php } ?>
</ul>

<div class="pagination"><?php echo $pagination; ?></div>


<?php } else { ?>
<div class="content"><?php echo $text_no_reviews; ?></div>
<?php } ?>
