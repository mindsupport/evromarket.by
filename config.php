<?php
// HTTP
define('HTTP_SERVER', 'http://evromarket/');

// HTTPS
define('HTTPS_SERVER', 'http://evromarket/');

// DIR
define('DIR_APPLICATION', '/home/id/GoogleDrive/Projects/evromarket/catalog/');
define('DIR_SYSTEM', '/home/id/GoogleDrive/Projects/evromarket/system/');
define('DIR_DATABASE', '/home/id/GoogleDrive/Projects/evromarket/system/database/');
define('DIR_LANGUAGE', '/home/id/GoogleDrive/Projects/evromarket/catalog/language/');
define('DIR_TEMPLATE', '/home/id/GoogleDrive/Projects/evromarket/catalog/view/theme/');
define('DIR_CONFIG', '/home/id/GoogleDrive/Projects/evromarket/system/config/');
define('DIR_IMAGE', '/home/id/GoogleDrive/Projects/evromarket/image/');
define('DIR_CACHE', '/home/id/GoogleDrive/Projects/evromarket/system/cache/');
define('DIR_DOWNLOAD', '/home/id/GoogleDrive/Projects/evromarket/download/');
define('DIR_LOGS', '/home/id/GoogleDrive/Projects/evromarket/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'evromarket');
define('DB_PREFIX', '');
?>